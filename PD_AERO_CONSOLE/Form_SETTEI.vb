﻿'=============================================================================================
'   
'
'
'   従業員情報関連　詳細情報入力
'
'
'
'=============================================================================================


Imports System.IO

Public Class Form_SETTEI


    '=======================================================================================================
    '   初期化
    '=======================================================================================================
    Private Sub FormJyugyo_Dtail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim fileno As Integer
        Dim xname As String = ""
        Dim xdat As String = ""
        Dim xdat2 As String = ""
        Dim strTemp4 As String = ""
        Dim arr() As String

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            T1.Text = arr(0)
            For i = 2 To 9
                CType(Me.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            T10.Text = arr(9)
            For i = 11 To 13
                CType(Me.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            For i = 14 To 18
                CType(Me.GroupBox1.Controls("T" & i), TextBox).Text = arr(i - 1)
            Next

            ' Show all available COM ports.
            T2.Items.Clear()
            For Each sp As String In My.Computer.Ports.SerialPortNames
                T2.Items.Add(sp)
            Next
            T2.Sorted = True

        End If


    End Sub



    '=======================================================================================================
    '   戻る
    '=======================================================================================================
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Me.Close()
    End Sub


    '=======================================================================================================
    '   設定を保存
    '=======================================================================================================
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim path As String = ""
        Dim fileno As Integer
        Dim xdat As String

        'ダイアログを表示する
        path = "SETTEI.txt"
        'OKボタンがクリックされたとき、選択されたファイル名を表示する

        fileno = FreeFile()
        FileOpen(fileno, path, OpenMode.Output)
        xdat = T1.Text
        For i = 2 To 9
            xdat = xdat + "," + CType(Me.GroupBox1.Controls("T" & i), ComboBox).Text
        Next
        xdat = xdat + "," + T10.Text
        For i = 11 To 13
            xdat = xdat + "," + CType(Me.GroupBox1.Controls("T" & i), ComboBox).Text
        Next
        For i = 14 To 18
            xdat = xdat + "," + CType(Me.GroupBox1.Controls("T" & i), TextBox).Text
        Next
        xdat = xdat + "," '以下予備確保
        xdat = xdat + ","
        xdat = xdat + ","
        xdat = xdat + ","
        xdat = xdat + ","
        xdat = xdat + ","
        xdat = xdat + ","
        xdat = xdat + "," + Format(Now, "yyyy_MM_dd")
        PrintLine(fileno, xdat)
        FileClose(fileno)

        For i = 1 To 14
            CType(Form_CONSOLE.Controls("L" & i), Label).Text = T15.Text + "～" + T16.Text
        Next
        Form_CONSOLE.L15.Text = "カウンタ " + T17.Text + "～" + T18.Text
        Form_CONSOLE.L16.Text = "カウンタ " + T17.Text + "～" + T18.Text

        MsgBox("設定を保存しました", MessageBoxIcon.Information, "完了")

    End Sub
End Class