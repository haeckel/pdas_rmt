﻿'=============================================================================================
'   
'
'
'   宇宙の仕事
'
'
'
'=============================================================================================
Imports System.IO
Imports System.IO.Ports
Imports System.Text


Public Class Form_CONSOLE

    Dim xx_Panel As Integer

    'システム異常
    Const Err_S_Watchdog As String = "E001"
    Const Err_S_24Volt As String = "E002"
    Const Err_S_6Volt As String = "E003"
    Const Err_S_EMGcut As String = "E004"
    'エンジン異常
    Const Err_E_Gas As String = "E101"
    Const Err_E_Fire As String = "E102"
    Const Err_E_Klow As String = "E103"
    Const Err_E_OverH1 As String = "E111"
    Const Err_E_OverH2 As String = "E112"
    Const Err_E_OverH3 As String = "E113"
    Const Err_E_OverH4 As String = "E114"
    Const Err_E_OverH5 As String = "E115"
    '通信コマンド異常1
    Const A_Com1_CSum As String = "E201"
    Const A_Com1_CRet As String = "E202"
    Const B_Com1_CSum As String = "E211"
    Const B_Com1_CRet As String = "E212"
    Const C_Com1_CSum As String = "E221"
    Const C_Com1_CRet As String = "E222"
    Const D_Com1_CSum As String = "E231"
    Const D_Com1_CRet As String = "E232"
    Const E_Com1_CSum As String = "E241"
    Const E_Com1_CRet As String = "E242"
    Const F_Com1_CSum As String = "E251"
    Const F_Com1_CRet As String = "E252"
    Const G_Com1_CSum As String = "E261"
    Const G_Com1_CRet As String = "E262"
    Const H_Com1_CSum As String = "E271"
    Const H_Com1_CRet As String = "E272"
    Const I_Com1_CSum As String = "E281"
    Const I_Com1_CRet As String = "E282"
    Const J_Com1_CSum As String = "E291"
    Const J_Com1_CRet As String = "E292"
    '通信コマンド異常2
    Const a_Com2_CSum As String = "E301"
    Const a_Com2_CRet As String = "E302"
    Const b_Com2_CSum As String = "E311"
    Const b_Com2_CRet As String = "E312"
    Const c_Com2_CSum As String = "E321"
    Const c_Com2_CRet As String = "E322"
    Const d_Com2_CSum As String = "E331"
    Const d_Com2_CRet As String = "E332"
    Const e_Com2_CSum As String = "E341"
    Const e_Com2_CRet As String = "E342"
    Const f_Com2_CSum As String = "E351"
    Const f_Com2_CRet As String = "E352"
    Const g_Com2_CSum As String = "E361"
    Const g_Com2_CRet As String = "E362"
    Const h_Com2_CSum As String = "E371"
    Const h_Com2_CRet As String = "E372"
    Const i_Com2_CSum As String = "E381"
    Const i_Com2_CRet As String = "E382"
    Const j_Com2_CSum As String = "E391"
    Const j_Com2_CRet As String = "E392"
    Const k_Com2_CSum As String = "E391"
    Const k_Com2_CRet As String = "E392"
    Const l_Com2_CSum As String = "E391"
    Const l_Com2_CRet As String = "E392"
    Const m_Com2_CSum As String = "E391"
    Const m_Com2_CRet As String = "E392"
    Const n_Com2_CSum As String = "E391"
    Const n_Com2_CRet As String = "E392"
    Const o_Com2_CSum As String = "E391"
    Const o_Com2_CRet As String = "E392"
    'RUNシーケンス異常
    Const o_EMR_Stop As String = "E401"

    '通信コマンド
    'バイナリ
    'Const Com_STX As Byte = &H02
    'Const Com_ETX As Byte = &H03
    'Const Com_ACK As Byte = &h06
    'Const Com_NAK As Byte = &h15

    '文字列
    Const Com_STX As String = "02"
    Const Com_ETX As String = "03"
    Const Com_ACK As String = "06"
    Const Com_NAK As String = "15"

    Dim SW1(10) As Integer
    Dim SW2(10) As Integer
    Dim LP1(10) As Integer
    Dim LP2(10) As Integer

    Dim RunExec As Integer

    '================================================
    '   通信クラス
    '================================================
    Private Class BuadRateItem
        Inherits Object

        Private m_name As String = ""
        Private m_value As Integer = 0

        '表示名称
        Public Property NAME() As String
            Set(ByVal value As String)
                m_name = value
            End Set
            Get
                Return m_name
            End Get
        End Property

        'ボーレート設定値.
        Public Property BAUDRATE() As Integer
            Set(ByVal value As Integer)
                m_value = value
            End Set
            Get
                Return m_value
            End Get
        End Property

        'コンボボックス表示用の文字列取得関数.
        Public Overrides Function ToString() As String
            Return m_name
        End Function

    End Class

    Private Class HandShakeItem
        Inherits Object

        Private m_name As String = ""
        Private m_value As Handshake = Handshake.None

        '表示名称
        Public Property NAME() As String
            Set(ByVal value As String)
                m_name = value
            End Set
            Get
                Return m_name
            End Get
        End Property

        '制御プロトコル設定値.
        Public Property HANDSHAKE() As Handshake
            Set(ByVal value As Handshake)
                m_value = value
            End Set
            Get
                Return m_value
            End Get
        End Property

        'コンボボックス表示用の文字列取得関数.
        Public Overrides Function ToString() As String
            Return m_name
        End Function

    End Class



    Dim a1 As Double
    Dim a2 As Double
    Dim a3 As Double
    Dim a4 As Double

    Public Property FormJyugyo As Object


    '================================================
    '   初期化
    '================================================
    Private Sub FormJyugyo_Choice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim fileno As Integer
        Dim xdat2 As String
        Dim arr() As String


        xAckBack = 1
        '保存パラメータ読み出し
        GetFolderName()

        '初期はテストモードにする
        PicRUN_Click(sender, e)
        RunExec = 0


        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            Form_SETTEI.T1.Text = arr(0)
            Label28.Text = arr(1)
            For i = 2 To 9
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            Form_SETTEI.T10.Text = arr(9)
            For i = 11 To 13
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            For i = 14 To 18
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), TextBox).Text = arr(i - 1)
            Next
        End If

        xSendCount = 0
        'ConnectButton_Click(sender, e) '通信開始
    End Sub


    '================================================
    '   アクティブ
    '================================================
    Private Sub FormJyugyo_Choice_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated

        Dim fileno As Integer
        Dim xdat2 As String
        Dim arr() As String

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            Form_SETTEI.T1.Text = arr(0)
            Label28.Text = arr(1)
            For i = 2 To 9
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            Form_SETTEI.T10.Text = arr(9)
            For i = 11 To 13
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            For i = 14 To 18
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), TextBox).Text = arr(i - 1)
            Next
        End If

    End Sub


    '================================================
    '   保存パラメータ読み出し
    '================================================
    Private Sub GetFolderName()
        Dim xdat As String
        Dim ydat As String
        Dim arr() As String
        Dim fileno As Integer
        Dim xdat2 As String

        ListBox1.Items.Clear()
        ListBox2.Items.Clear()

        Dim files As String() = System.IO.Directory.GetFiles(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")), "*.csv", System.IO.SearchOption.AllDirectories)

        ListBox2.Items.AddRange(files)

        If ListBox2.Items.Count < 1 Then Exit Sub

        For i = 0 To ListBox2.Items.Count - 1
            xdat = ListBox2.Items(i)
            ydat = Mid(xdat, InStrRev(xdat, "\") + 1, 10) + "   "
            Using read = New System.IO.StreamReader(xdat, System.Text.Encoding.Default, False)
                Dim strTemp As String = ""
                Dim strWrite As String = ""
                strTemp = read.ReadLine()
                strTemp = read.ReadLine()
                arr = strTemp.Split(",")
                ydat = ydat + arr(34) + Mid("                                   ", 1, 34 - Len(arr(34))) + arr(35)
                ListBox1.Items.Add(ydat)
                read.Close()
            End Using
        Next

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            For i = 1 To 14
                CType(Me.Controls("L" & i), Label).Text = arr(14) + "～" + arr(15)
            Next
            L15.Text = "カウンタ " + arr(16) + "～" + arr(17)
            L16.Text = "カウンタ " + arr(16) + "～" + arr(17)
        Else
            For i = 1 To 16
                CType(Me.Controls("L" & i), Label).Text = ""
            Next
        End If

    End Sub


    '================================================
    '   戻る
    '================================================
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        End
        'メッセージボックスを表示する 
        Dim result As DialogResult = MessageBox.Show("本当に終了してよろしいですか？",
                                             "確認",
                                             MessageBoxButtons.YesNoCancel,
                                             MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button2)

        '何が選択されたか調べる 
        If result = DialogResult.Yes Then
            '「はい」が選択された時 
            End
        ElseIf result = DialogResult.No Then
            '「いいえ」が選択された時 
            Exit Sub
        ElseIf result = DialogResult.Cancel Then
            '「キャンセル」が選択された時 
            Exit Sub
        End If
    End Sub


    '================================================
    '   パラメータ保存
    '================================================
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim path As String = ""
        Dim fileno As Integer
        Dim xdat As String

        If TextBox31.Text = "" Or TextBox1.Text = "" Then
            MsgBox("ファイル名とコメントを入力してください", MessageBoxIcon.Warning, "警告")
            Exit Sub
        End If

        'ダイアログを表示する
        path = Format(Now, "yyyy_MM_dd_") + TextBox31.Text + ".csv"
        'OKボタンがクリックされたとき、選択されたファイル名を表示する

        fileno = FreeFile()
        FileOpen(fileno, path, OpenMode.Output)
        xdat = "T2,T3,T4,T5,T6,T7,T8,T10,T11,T12,T13,T14,T15,T16,C1,T1,T9,T100,T102,T103,T104,T105,T106,T107,T108,T110,T111,T112,T113,T114,T115,T116,C2,T200,ファイル名,コメント"
        PrintLine(fileno, xdat)
        xdat = T2.Text
        For i = 3 To 8
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        For i = 10 To 16
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        xdat = xdat + "," + C1.Text
        xdat = xdat + "," + T1.Text
        xdat = xdat + "," + T9.Text
        xdat = xdat + "," + T100.Text
        For i = 102 To 108
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        For i = 110 To 116
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        xdat = xdat + "," + C2.Text
        xdat = xdat + "," + T200.Text
        xdat = xdat + "," + TextBox31.Text + "," + TextBox1.Text
        PrintLine(fileno, xdat)
        FileClose(fileno)

        MsgBox("ファイルを保存しました", MessageBoxIcon.Information, "完了")
        GetFolderName()

    End Sub


    '================================================
    '   設定画面へ
    '================================================
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Form_SETTEI.ShowDialog()

    End Sub


    '================================================
    '   パラメータ読み込みボタン
    '================================================
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim a As Integer
        Dim xdat As String
        Dim ydat As String
        Dim arr() As String

        If ListBox1.SelectedIndex < 0 Then Exit Sub

        a = ListBox1.SelectedIndex

        xdat = ListBox2.Items(a)
        ydat = Mid(xdat, InStrRev(xdat, "\") + 1, 10) + "   "
        Using read = New System.IO.StreamReader(xdat, System.Text.Encoding.Default, False)
            Dim strTemp As String = ""
            Dim strWrite As String = ""
            strTemp = read.ReadLine()
            strTemp = read.ReadLine()
            arr = strTemp.Split(",")
            read.Close()
        End Using

        TextBox31.Text = arr(34)
        TextBox1.Text = arr(35)
        For i = 2 To 8
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 2)
        Next
        For i = 10 To 16
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 3)
        Next
        C1.Text = arr(14)
        T1.Text = arr(15)
        T9.Text = arr(16)
        T100.Text = arr(17)
        For i = 102 To 108
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 84)
        Next
        For i = 110 To 116
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 85)
        Next
        C2.Text = arr(32)
        T200.Text = arr(33)

        Para_Check()

    End Sub


    '================================================
    '   パラメータリストのダブルクリック
    '================================================
    Private Sub ListBox1_DoubleClick(sender As Object, e As EventArgs) Handles ListBox1.DoubleClick
        'パラメータ読み込み処理へ
        Button3_Click(sender, e)
    End Sub


    '================================================
    '   入力欄のENTERキー移動処理
    '================================================
    Private Sub T2_KeyUp(sender As Object, e As KeyEventArgs) Handles T2.KeyUp
        If e.KeyCode = Keys.Enter Then T10.Focus()
    End Sub
    Private Sub T10_KeyUp(sender As Object, e As KeyEventArgs) Handles T10.KeyUp
        If e.KeyCode = Keys.Enter Then T3.Focus()
    End Sub
    Private Sub T3_KeyUp(sender As Object, e As KeyEventArgs) Handles T3.KeyUp
        If e.KeyCode = Keys.Enter Then T11.Focus()
    End Sub
    Private Sub T11_KeyUp(sender As Object, e As KeyEventArgs) Handles T11.KeyUp
        If e.KeyCode = Keys.Enter Then T4.Focus()
    End Sub
    Private Sub T4_KeyUp(sender As Object, e As KeyEventArgs) Handles T4.KeyUp
        If e.KeyCode = Keys.Enter Then T12.Focus()
    End Sub
    Private Sub T12_KeyUp(sender As Object, e As KeyEventArgs) Handles T12.KeyUp
        If e.KeyCode = Keys.Enter Then T5.Focus()
    End Sub
    Private Sub T5_KeyUp(sender As Object, e As KeyEventArgs) Handles T5.KeyUp
        If e.KeyCode = Keys.Enter Then T13.Focus()
    End Sub
    Private Sub T13_KeyUp(sender As Object, e As KeyEventArgs) Handles T13.KeyUp
        If e.KeyCode = Keys.Enter Then T6.Focus()
    End Sub
    Private Sub T6_KeyUp(sender As Object, e As KeyEventArgs) Handles T6.KeyUp
        If e.KeyCode = Keys.Enter Then T14.Focus()
    End Sub
    Private Sub T14_KeyUp(sender As Object, e As KeyEventArgs) Handles T14.KeyUp
        If e.KeyCode = Keys.Enter Then T7.Focus()
    End Sub
    Private Sub T7_KeyUp(sender As Object, e As KeyEventArgs) Handles T7.KeyUp
        If e.KeyCode = Keys.Enter Then T15.Focus()
    End Sub
    Private Sub T15_KeyUp(sender As Object, e As KeyEventArgs) Handles T15.KeyUp
        If e.KeyCode = Keys.Enter Then T8.Focus()
    End Sub
    Private Sub T8_KeyUp(sender As Object, e As KeyEventArgs) Handles T8.KeyUp
        If e.KeyCode = Keys.Enter Then T16.Focus()
    End Sub
    Private Sub T16_KeyUp(sender As Object, e As KeyEventArgs) Handles T16.KeyUp
        If e.KeyCode = Keys.Enter Then C1.Focus()
    End Sub
    Private Sub C1_KeyUp(sender As Object, e As KeyEventArgs) Handles C1.KeyUp
        If e.KeyCode = Keys.Enter Then T1.Focus()
    End Sub
    Private Sub T1_KeyUp(sender As Object, e As KeyEventArgs) Handles T1.KeyUp
        If e.KeyCode = Keys.Enter Then T9.Focus()
    End Sub
    Private Sub T9_KeyUp(sender As Object, e As KeyEventArgs) Handles T9.KeyUp
        If e.KeyCode = Keys.Enter Then T100.Focus()
    End Sub
    Private Sub T100_KeyUp(sender As Object, e As KeyEventArgs) Handles T100.KeyUp
        If e.KeyCode = Keys.Enter Then T102.Focus()
    End Sub

    Private Sub T102_KeyUp(sender As Object, e As KeyEventArgs) Handles T102.KeyUp
        If e.KeyCode = Keys.Enter Then T110.Focus()
    End Sub
    Private Sub T110_KeyUp(sender As Object, e As KeyEventArgs) Handles T110.KeyUp
        If e.KeyCode = Keys.Enter Then T103.Focus()
    End Sub
    Private Sub T103_KeyUp(sender As Object, e As KeyEventArgs) Handles T103.KeyUp
        If e.KeyCode = Keys.Enter Then T111.Focus()
    End Sub
    Private Sub T111_KeyUp(sender As Object, e As KeyEventArgs) Handles T111.KeyUp
        If e.KeyCode = Keys.Enter Then T104.Focus()
    End Sub
    Private Sub T104_KeyUp(sender As Object, e As KeyEventArgs) Handles T104.KeyUp
        If e.KeyCode = Keys.Enter Then T112.Focus()
    End Sub
    Private Sub T112_KeyUp(sender As Object, e As KeyEventArgs) Handles T112.KeyUp
        If e.KeyCode = Keys.Enter Then T105.Focus()
    End Sub
    Private Sub T105_KeyUp(sender As Object, e As KeyEventArgs) Handles T105.KeyUp
        If e.KeyCode = Keys.Enter Then T113.Focus()
    End Sub
    Private Sub T113_KeyUp(sender As Object, e As KeyEventArgs) Handles T113.KeyUp
        If e.KeyCode = Keys.Enter Then T106.Focus()
    End Sub
    Private Sub T106_KeyUp(sender As Object, e As KeyEventArgs) Handles T106.KeyUp
        If e.KeyCode = Keys.Enter Then T114.Focus()
    End Sub
    Private Sub T114_KeyUp(sender As Object, e As KeyEventArgs) Handles T114.KeyUp
        If e.KeyCode = Keys.Enter Then T107.Focus()
    End Sub
    Private Sub T107_KeyUp(sender As Object, e As KeyEventArgs) Handles T107.KeyUp
        If e.KeyCode = Keys.Enter Then T115.Focus()
    End Sub
    Private Sub T115_KeyUp(sender As Object, e As KeyEventArgs) Handles T115.KeyUp
        If e.KeyCode = Keys.Enter Then T108.Focus()
    End Sub
    Private Sub T108_KeyUp(sender As Object, e As KeyEventArgs) Handles T108.KeyUp
        If e.KeyCode = Keys.Enter Then T116.Focus()
    End Sub
    Private Sub T116_KeyUp(sender As Object, e As KeyEventArgs) Handles T116.KeyUp
        If e.KeyCode = Keys.Enter Then C2.Focus()
    End Sub
    Private Sub C2_KeyUp(sender As Object, e As KeyEventArgs) Handles C2.KeyUp
        If e.KeyCode = Keys.Enter Then T200.Focus()
    End Sub
    Private Sub T200_KeyUp(sender As Object, e As KeyEventArgs) Handles T200.KeyUp
        If e.KeyCode = Keys.Enter Then TextBox31.Focus()
    End Sub
    Private Sub TextBox31_KeyUp(sender As Object, e As KeyEventArgs) Handles TextBox31.KeyUp
        If e.KeyCode = Keys.Enter Then TextBox1.Focus()
    End Sub




    '================================================
    '   STARTボタン
    '================================================
    Private Sub Label73_DoubleClick(sender As Object, e As EventArgs) Handles Label_START.DoubleClick
        If CK_KEY.Checked = True Then Exit Sub
        If PicTEST.Visible = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        If Label_START.Text = "OFF" Then
            Label_START.BackColor = Color.Yellow
            Label_START.Text = "ON"
            Timer1.Enabled = True
            SW1(0) = 1
            RunExec = 1
        Else
            Label_START.BackColor = Color.Gray
            Label_START.Text = "OFF"
            Timer1.Enabled = False
            Timer2.Enabled = False
            SW1(0) = 0
            RunExec = 0
        End If
        set_Panel()
    End Sub


    '================================================
    '   STARTボタン点滅用タイマ
    '================================================
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If PicTEST.Visible = True Then Exit Sub
        Label_START.BackColor = Color.Yellow
        Timer2.Enabled = True
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Label_START.BackColor = Color.Gray
        Timer2.Enabled = False
    End Sub


    '================================================
    '   エマージェンシボタン押下
    '================================================
    Private Sub Pic_EMMR1_Click(sender As Object, e As EventArgs) Handles Pic_EMMR1.Click
        Timer3.Enabled = True
        SW1(1) = 1
        set_Panel()
        RunExec = 0
    End Sub
    Private Sub Pic_EMMR2_Click(sender As Object, e As EventArgs) Handles Pic_EMMR2.Click
        Timer3.Enabled = True
        SW1(1) = 1
        set_Panel()
        RunExec = 0
    End Sub


    '================================================
    '   AIR-A
    '================================================
    Private Sub Label76_MouseDown(sender As Object, e As MouseEventArgs) Handles Label76.MouseDown
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label76.BackColor = Color.MidnightBlue
        Label76.Text = "ON"
        SW1(2) = 1
        set_Panel()
    End Sub
    Private Sub Label76_MouseUp(sender As Object, e As MouseEventArgs) Handles Label76.MouseUp
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label76.BackColor = Color.Cyan
        Label76.Text = "OFF"
        SW1(2) = 0
        set_Panel()
    End Sub


    '================================================
    '   AIR-B(O2)
    '================================================
    Private Sub Label45_MouseDown(sender As Object, e As MouseEventArgs) Handles Label45.MouseDown
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label45.BackColor = Color.MidnightBlue
        Label45.Text = "ON"
        SW1(3) = 1
        set_Panel()
    End Sub
    Private Sub Label45_MouseUp(sender As Object, e As MouseEventArgs) Handles Label45.MouseUp
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label45.BackColor = Color.Cyan
        Label45.Text = "OFF"
        SW1(3) = 0
        set_Panel()
    End Sub


    '================================================
    '   N2
    '================================================
    Private Sub Label78_MouseDown(sender As Object, e As MouseEventArgs) Handles Label78.MouseDown
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label78.BackColor = Color.DarkGreen
        Label78.Text = "ON"
        SW1(4) = 1
        set_Panel()
    End Sub
    Private Sub Label78_MouseUp(sender As Object, e As MouseEventArgs) Handles Label78.MouseUp
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label78.BackColor = Color.Lime
        Label78.Text = "OFF"
        SW1(4) = 0
        set_Panel()
    End Sub


    '================================================
    '   LPG
    '================================================
    Private Sub Label77_MouseDown(sender As Object, e As MouseEventArgs) Handles Label77.MouseDown
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label77.BackColor = Color.Gray
        Label77.Text = "ON"
        SW1(5) = 1
        set_Panel()
    End Sub
    Private Sub Label77_MouseUp(sender As Object, e As MouseEventArgs) Handles Label77.MouseUp
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        Label77.BackColor = Color.Yellow
        Label77.Text = "OFF"
        SW1(5) = 0
        set_Panel()
    End Sub


    '================================================
    '   RUNモードからTESTモードに切り替え
    '================================================
    Private Sub PicRUN_Click(sender As Object, e As EventArgs) Handles PicRUN.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PicTEST.Visible = True
        PicRUN.Visible = False
        Label76.BackColor = Color.Cyan
        Label45.BackColor = Color.Cyan
        Label78.BackColor = Color.Lime
        Label77.BackColor = Color.Yellow
        If Label_START.Text = "ON" Then
            Timer1.Enabled = False
            Timer2.Enabled = False
            Label_START.BackColor = Color.Gray
            Label_START.Text = "OFF"
            SW1(0) = 0
        End If
        Timer3.Enabled = False
        Pic_EMMR1.Visible = False
        Pic_EMMR2.Visible = False
        SW1(1) = 0
        SW1(6) = 0
        set_Panel()
    End Sub


    '================================================
    '   TESTモードからRUNモードに切り替え
    '================================================
    Private Sub PicTEST_Click(sender As Object, e As EventArgs) Handles PicTEST.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PicRUN.Visible = True
        PicTEST.Visible = False
        Label76.BackColor = Color.MidnightBlue
        Label45.BackColor = Color.MidnightBlue
        Label78.BackColor = Color.DarkGreen
        Label77.BackColor = Color.Gray
        Pic_EMMR1.Visible = False
        Pic_EMMR2.Visible = True
        SW1(6) = 1
        set_Panel()
    End Sub


    '================================================
    '   PURGE
    '================================================
    Private Sub PB_P_D_Click(sender As Object, e As EventArgs) Handles PB_P_D.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PB_P_U.Visible = True
        PB_P_D.Visible = False
        SW1(7) = 1
        set_Panel()
    End Sub
    Private Sub PB_P_U_Click(sender As Object, e As EventArgs) Handles PB_P_U.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PB_P_D.Visible = True
        PB_P_U.Visible = False
        SW1(7) = 0
        set_Panel()
    End Sub


    '================================================
    '   PDJE 切り替え
    '================================================
    Private Sub PB_JE_D_Click(sender As Object, e As EventArgs) Handles PB_JE_D.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PB_JE_U.Visible = True
        PB_JE_D.Visible = False
        Label79.BackColor = Color.Lime
        SW1(8) = 1
        set_Panel()
    End Sub
    Private Sub PB_JE_U_Click(sender As Object, e As EventArgs) Handles PB_JE_U.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PB_JE_D.Visible = True
        PB_JE_U.Visible = False
        Label79.BackColor = Color.DarkGreen
        SW1(8) = 0
        set_Panel()
    End Sub


    '================================================
    '   PDRE 切り替え
    '================================================
    Private Sub PB_RE_D_Click(sender As Object, e As EventArgs) Handles PB_RE_D.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PB_RE_U.Visible = True
        PB_RE_D.Visible = False
        Label80.BackColor = Color.Lime
        SW1(9) = 1
        set_Panel()
    End Sub
    Private Sub PB_RE_U_Click(sender As Object, e As EventArgs) Handles PB_RE_U.Click
        If CK_KEY.Checked = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        PB_RE_D.Visible = True
        PB_RE_U.Visible = False
        Label80.BackColor = Color.DarkGreen
        SW1(9) = 0
        set_Panel()
    End Sub


    '================================================
    '   PUMP
    '================================================
    Private Sub Label24_DoubleClick(sender As Object, e As EventArgs) Handles Label24.DoubleClick
        If CK_KEY.Checked = True Then Exit Sub
        If PicRUN.Visible = True Then Exit Sub
        If RunExec = 1 Then Exit Sub
        If Label24.Text = "OFF" Then
            Label24.BackColor = Color.Yellow
            Label24.Text = "ON"
            SW1(10) = 1
        Else
            Label24.BackColor = Color.Gray
            Label24.Text = "OFF"
            SW1(10) = 0
        End If
        set_Panel()
    End Sub






    '================================================
    '   エマージェンシーボタン点滅
    '================================================
    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        If Pic_EMMR1.Visible = False Then
            Pic_EMMR1.Visible = True
            Pic_EMMR2.Visible = False
        Else
            Pic_EMMR2.Visible = True
            Pic_EMMR1.Visible = False
        End If
    End Sub


    '================================================
    '   パネル設定（スイッチ）
    '================================================
    Private Sub set_Panel()
        Dim xSUM1U As Integer = 0
        Dim xSUM1L As Integer = 0
        Dim cSUM1 As String = ""
        Dim cSUM2 As String = ""

        xSUM1L = SW1(0) + (SW1(1) * 2) + (SW1(2) * 4) + (SW1(3) * 8) + (SW1(4) * 16) + (SW1(5) * 32) + (SW1(6) * 64) + (SW1(7) * 128)
        xSUM1U = SW1(8) + (SW1(9) * 2) + (SW1(10) * 4)
        'xSUM2 = SW2(0) + (SW2(1) * 2) + (SW2(2) * 4) + (SW2(3) * 8) + (SW2(4) * 16) + (SW2(5) * 32) + (SW2(6) * 64) + (SW2(7) * 128) + (SW2(8) * 256)

        cSUM1 = xSUM1U.ToString("X2") + xSUM1L.ToString("X2")
        LabelSUM1.Text = cSUM1

        Com_SendDenbun("4A02" + cSUM1) 'J送信

        xx_Disp()

    End Sub

    Private Sub xx_Disp()
        Dim xdat = "〇〇〇〇〇"
        If SW1(10) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(9) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(8) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        Label_BIT2.Text = xdat

        xdat = ""
        If SW1(7) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(6) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(5) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(4) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(3) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(2) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(1) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If SW1(0) = 1 Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        Label_BIT.Text = xdat
    End Sub

    '================================================
    '   パネル設定（ランプ）
    '================================================
    Private Sub set_Lump(Y1 As String, Y2 As String, Y3 As String)
        Dim xDat As String = ""

        LabelSUM2.Text = Y1

        xx_Disp2(Y1)

    End Sub

    Private Sub xx_Disp2(xDat As String)
        Dim xBIT1 As String
        Dim xBIT2 As String
        Dim xSUM As Integer

        xBIT1 = "〇〇〇〇〇〇〇"
        xSUM = Convert.ToInt32(Mid(xDat, 2, 1), 16)
        If xSUM And &H1 Then
            xBIT1 = xBIT1 + "●"
            Label24.BackColor = Color.Yellow
        Else
            xBIT1 = xBIT1 + "〇"
            Label24.BackColor = Color.Gray
        End If
        Label_BIT4.Text = xBIT1

        xBIT2 = ""
        xSUM = Convert.ToInt32(Mid(xDat, 3, 1), 16)
        If xSUM And &H8 Then
            xBIT2 = xBIT2 + "●"
            Label80.BackColor = Color.Lime
        Else
            xBIT2 = xBIT2 + "〇"
            Label80.BackColor = Color.DarkGreen
        End If
        If xSUM And &H4 Then
            xBIT2 = xBIT2 + "●"
            Label79.BackColor = Color.Lime
        Else
            xBIT2 = xBIT2 + "〇"
            Label79.BackColor = Color.DarkGreen
        End If
        If xSUM And &H2 Then
            xBIT2 = xBIT2 + "●"
            Label_START.BackColor = Color.Yellow
        Else
            xBIT2 = xBIT2 + "〇"
            Label_START.BackColor = Color.Gray
        End If
        If xSUM And &H1 Then
            xBIT2 = xBIT2 + "●"
            Label77.BackColor = Color.Gray
            Label77.Text = "ON"
        Else
            xBIT2 = xBIT2 + "〇"
            Label77.BackColor = Color.Yellow
            Label77.Text = "OFF"
        End If

        xSUM = Convert.ToInt32(Mid(xDat, 4, 1), 16)
        If xSUM And &H8 Then
            xBIT2 = xBIT2 + "●"
            Label78.BackColor = Color.Gray
            Label78.Text = "ON"
        Else
            xBIT2 = xBIT2 + "〇"
            Label78.BackColor = Color.Lime
            Label78.Text = "OFF"
        End If
        If xSUM And &H4 Then
            xBIT2 = xBIT2 + "●"
            Label45.BackColor = Color.Gray
            Label45.Text = "ON"
        Else
            xBIT2 = xBIT2 + "〇"
            Label45.BackColor = Color.Cyan
            Label45.Text = "OFF"
        End If
        If xSUM And &H2 Then
            xBIT2 = xBIT2 + "●"
            Label76.BackColor = Color.Gray
            Label76.Text = "ON"
        Else
            xBIT2 = xBIT2 + "〇"
            Label76.BackColor = Color.Cyan
            Label76.Text = "OFF"
        End If
        If xSUM And &H1 Then
            xBIT2 = xBIT2 + "●"
            Timer3.Enabled = True
            Pic_EMMR1.Visible = False
            Pic_EMMR2.Visible = True
        Else
            xBIT2 = xBIT2 + "〇"
            Timer3.Enabled = False
            Pic_EMMR1.Visible = True
            Pic_EMMR2.Visible = False
        End If
        Label_BIT3.Text = xBIT2


    End Sub

    '================================================
    '   パラメータ送信　ＪＥＴ－ＭＯＤＥ
    '================================================
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim xT1 As Integer
        Dim xT2 As Integer

        If Para_Check() Then
            'メッセージボックスを表示する 
            Dim result As DialogResult = MessageBox.Show("上下限値範囲外がありますが、送信してよろしいですか？",
                                             "質問",
                                             MessageBoxButtons.YesNoCancel,
                                             MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button2)

            '何が選択されたか調べる 
            If result = DialogResult.Yes Then
                '「はい」が選択された時 
            ElseIf result = DialogResult.No Then
                '「いいえ」が選択された時 
                Exit Sub
            ElseIf result = DialogResult.Cancel Then
                '「キャンセル」が選択された時 
                Exit Sub
            End If
        End If

        xAckBack = 0
        T2.BackColor = Color.Yellow
        T10.BackColor = Color.Yellow
        xT1 = Val(T2.Text) * 10
        xT2 = Val(T10.Text) * 10
        Com_SendDenbun("4104" + xT1.ToString("X4") + xT2.ToString("X4")) 'Aを返す

        xAckBack = 0
        T3.BackColor = Color.Yellow
        T11.BackColor = Color.Yellow
        xT1 = Val(T3.Text) * 10
        xT2 = Val(T11.Text) * 10
        Com_SendDenbun("4204" + xT1.ToString("X4") + xT2.ToString("X4")) 'Bを返す

        xAckBack = 0
        T4.BackColor = Color.Yellow
        T12.BackColor = Color.Yellow
        xT1 = Val(T4.Text) * 10
        xT2 = Val(T12.Text) * 10
        Com_SendDenbun("4304" + xT1.ToString("X4") + xT2.ToString("X4")) 'Cを返す

        xAckBack = 0
        T5.BackColor = Color.Yellow
        T13.BackColor = Color.Yellow
        xT1 = Val(T5.Text) * 10
        xT2 = Val(T13.Text) * 10
        Com_SendDenbun("4404" + xT1.ToString("X4") + xT2.ToString("X4")) 'Dを返す

        xAckBack = 0
        T6.BackColor = Color.Yellow
        T14.BackColor = Color.Yellow
        xT1 = Val(T6.Text) * 10
        xT2 = Val(T14.Text) * 10
        Com_SendDenbun("4504" + xT1.ToString("X4") + xT2.ToString("X4")) 'Eを返す

        xAckBack = 0
        T7.BackColor = Color.Yellow
        T15.BackColor = Color.Yellow
        xT1 = Val(T7.Text) * 10
        xT2 = Val(T15.Text) * 10
        Com_SendDenbun("4604" + xT1.ToString("X4") + xT2.ToString("X4")) 'Fを返す

        xAckBack = 0
        T8.BackColor = Color.Yellow
        T16.BackColor = Color.Yellow
        xT1 = Val(T8.Text) * 10
        xT2 = Val(T16.Text) * 10
        Com_SendDenbun("4704" + xT1.ToString("X4") + xT2.ToString("X4")) 'Gを返す

        xAckBack = 0
        C1.BackColor = Color.Yellow
        xT1 = Val(C1.Text)
        Com_SendDenbun("4802" + xT1.ToString("X4")) 'Hを返す

        xAckBack = 0
        T1.BackColor = Color.Yellow
        T9.BackColor = Color.Yellow
        xT1 = Val(T1.Text) * 10
        xT2 = Val(T9.Text) * 10
        Com_SendDenbun("5504" + xT1.ToString("X4") + xT2.ToString("X4")) 'Uを返す

        xAckBack = 0
        T100.BackColor = Color.Yellow
        xT1 = Val(T100.Text) * 10
        Com_SendDenbun("5602" + xT1.ToString("X4")) 'Vを返す
        xAckBack = 0

        MsgBox("送信しました。", MessageBoxIcon.Information, "実行")

    End Sub


    '================================================
    '   パラメータ送信　ＲＯＣＫＥＴ－ＭＯＤＥ
    '================================================
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim xT1 As Integer
        Dim xT2 As Integer

        If Para_Check() Then
            'メッセージボックスを表示する 
            Dim result As DialogResult = MessageBox.Show("上下限値範囲外がありますが、送信してよろしいですか？",
                                             "質問",
                                             MessageBoxButtons.YesNoCancel,
                                             MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button2)

            '何が選択されたか調べる 
            If result = DialogResult.Yes Then
                '「はい」が選択された時 
            ElseIf result = DialogResult.No Then
                '「いいえ」が選択された時 
                Exit Sub
            ElseIf result = DialogResult.Cancel Then
                '「キャンセル」が選択された時 
                Exit Sub
            End If
        End If

        xAckBack = 0
        T102.BackColor = Color.Yellow
        T110.BackColor = Color.Yellow
        xT1 = Val(T102.Text) * 10
        xT2 = Val(T110.Text) * 10
        Com_SendDenbun("4B04" + xT1.ToString("X4") + xT2.ToString("X4")) 'Kを返す

        xAckBack = 0
        T103.BackColor = Color.Yellow
        T111.BackColor = Color.Yellow
        xT1 = Val(T103.Text) * 10
        xT2 = Val(T111.Text) * 10
        Com_SendDenbun("4C04" + xT1.ToString("X4") + xT2.ToString("X4")) 'Lを返す

        xAckBack = 0
        T104.BackColor = Color.Yellow
        T112.BackColor = Color.Yellow
        xT1 = Val(T104.Text) * 10
        xT2 = Val(T112.Text) * 10
        Com_SendDenbun("5004" + xT1.ToString("X4") + xT2.ToString("X4")) 'Pを返す

        xAckBack = 0
        T105.BackColor = Color.Yellow
        T113.BackColor = Color.Yellow
        xT1 = Val(T105.Text) * 10
        xT2 = Val(T113.Text) * 10
        Com_SendDenbun("5104" + xT1.ToString("X4") + xT2.ToString("X4")) 'Qを返す

        xAckBack = 0
        T106.BackColor = Color.Yellow
        T114.BackColor = Color.Yellow
        xT1 = Val(T106.Text) * 10
        xT2 = Val(T114.Text) * 10
        Com_SendDenbun("5204" + xT1.ToString("X4") + xT2.ToString("X4")) 'Rを返す

        xAckBack = 0
        T107.BackColor = Color.Yellow
        T115.BackColor = Color.Yellow
        xT1 = Val(T107.Text) * 10
        xT2 = Val(T115.Text) * 10
        Com_SendDenbun("5304" + xT1.ToString("X4") + xT2.ToString("X4")) 'Sを返す

        xAckBack = 0
        T108.BackColor = Color.Yellow
        T116.BackColor = Color.Yellow
        xT1 = Val(T108.Text) * 10
        xT2 = Val(T116.Text) * 10
        Com_SendDenbun("5404" + xT1.ToString("X4") + xT2.ToString("X4")) 'Tを返す

        xAckBack = 0
        C2.BackColor = Color.Yellow
        xT1 = Val(C2.Text)
        Com_SendDenbun("4902" + xT1.ToString("X4")) 'Iを返す

        xAckBack = 0
        T200.BackColor = Color.Yellow
        xT1 = Val(T200.Text) * 10
        Com_SendDenbun("5702" + xT1.ToString("X4")) 'Wを返す
        xAckBack = 0

        MsgBox("送信しました。", MessageBoxIcon.Information, "実行")

    End Sub


    '================================================
    '   パラメータチェック
    '================================================
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If Para_Check() Then
            MsgBox("赤い部分をご確認ください。", MessageBoxIcon.Error, "範囲外あり")
        Else
            MsgBox("全て範囲内です。", MessageBoxIcon.Information, "範囲外なし")
        End If
    End Sub

    Private Function Para_Check()
        Dim fileno As Integer
        Dim xdat2 As String
        Dim arr() As String

        Para_Check = 0

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
        Else
            MsgBox("先に設定画面で上下限値を保存してください", MessageBoxIcon.Stop, "警告")
            Exit Function
        End If

        a1 = Val(arr(14))
        a2 = Val(arr(15))
        a3 = Val(arr(16))
        a4 = Val(arr(17))

        For i = 2 To 8
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        For i = 10 To 16
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        C1.BackColor = Color.White
        T1.BackColor = Color.White
        T9.BackColor = Color.White
        T100.BackColor = Color.White

        For i = 102 To 108
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        For i = 110 To 116
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        C2.BackColor = Color.White
        T200.BackColor = Color.White

        For i = 2 To 8
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        For i = 10 To 16
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        If In_CheckCNT(C1.Text) Then
            C1.BackColor = Color.Red
            Para_Check = 1
        End If
        If In_CheckTIM(T1.Text) Then
            T1.BackColor = Color.Red
            Para_Check = 1
        End If
        If In_CheckTIM(T9.Text) Then
            T9.BackColor = Color.Red
            Para_Check = 1
        End If
        If In_CheckTIM(T100.Text) Then
            T100.BackColor = Color.Red
            Para_Check = 1
        End If

        For i = 102 To 108
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        For i = 110 To 116
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        If In_CheckCNT(C2.Text) Then
            C2.BackColor = Color.Red
            Para_Check = 1
        End If
        If In_CheckTIM(T200.Text) Then
            T200.BackColor = Color.Red
            Para_Check = 1
        End If

    End Function

    Private Function In_CheckTIM(xdat As String)
        Dim a As Double
        Dim ydat As String

        In_CheckTIM = 0
        If InStr(xdat, ".") > 0 Then
            ydat = Mid(xdat, InStr(xdat, ".") + 1)
            If Len(ydat) <> 1 Then
                In_CheckTIM = 1
                Exit Function
            End If
        End If
        a = Val(xdat)
        If a < a1 Then
            In_CheckTIM = 1
            Exit Function
        End If
        If a > a2 Then
            In_CheckTIM = 1
            Exit Function
        End If
    End Function

    Private Function In_CheckCNT(xdat As String)
        Dim a As Double

        In_CheckCNT = 0
        If InStr(xdat, ".") > 0 Then
            In_CheckCNT = 1
            Exit Function
        End If
        a = Val(xdat)
        If a < a3 Then
            In_CheckCNT = 1
            Exit Function
        End If
        If a > a4 Then
            In_CheckCNT = 1
            Exit Function
        End If
    End Function




    '================================================
    '   シリアルポートオープン
    '================================================
    Private Sub Com_Open()

        If SerialPort1.IsOpen = True Then

            'シリアルポートをクローズする.
            SerialPort1.Close()

            'ボタンの表示を[切断]から[接続]に変える.
            ConnectButton.Text = "接続"
        Else

            'オープンするシリアルポートをコンボボックスから取り出す.
            SerialPort1.PortName = Form_SETTEI.T2.SelectedItem.ToString()

            'ボーレートをコンボボックスから取り出す.
            Dim baud As BuadRateItem
            baud = Form_SETTEI.T3.SelectedItem
            SerialPort1.BaudRate = baud.BAUDRATE

            'データビットをセットする. (データビット = 8ビット)
            SerialPort1.DataBits = 8

            'パリティビットをセットする. (パリティビット = なし)
            SerialPort1.Parity = Parity.None

            'ストップビットをセットする. (ストップビット = 1ビット)
            SerialPort1.StopBits = StopBits.One

            '文字コードをセットする.
            SerialPort1.Encoding = Encoding.Unicode

            Try
                'シリアルポートをオープンする.
                SerialPort1.Open()

                'ボタンの表示を[接続]から[切断]に変える.
                ConnectButton.Text = "切断"
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If

    End Sub


    '================================================
    '   シリアル送信前処理（電文前後にSTXとETX、最後にチェックサムを追加）
    '================================================
    Private Sub Com_SendDenbun(aDENBUN As String)
        '文字列からSTXとETX、チェックサムを追加して送信電文を作り送信する
        Dim ySUM As Integer
        Dim xdat As String
        Log("送信生: " + aDENBUN)

        aDENBUN = Com_STX + aDENBUN + Com_ETX 'STXとETXを付加する

        'チェックサムを計算する
        ySUM = 0
        xdat = ""
        For i = 1 To Len(aDENBUN) Step 2
            xdat = Mid(aDENBUN, i, 2)
            ySUM = ySUM + Convert.ToInt32(xdat, 16) '16進数文字列を数値へ
        Next
        ySUM = ySUM Mod 256
        SndTextBox.Text = aDENBUN + ySUM.ToString("X2") '数値を16進数2桁文字列へ
        Log("送信フォーマット: " + SndTextBox.Text)
        Com_Send() '送信する

xReturn:
        If xAckBack = 1 Then Exit Sub
        Application.DoEvents()
        GoTo xReturn

    End Sub


    '================================================
    '   シリアル送信（テキストボックスを再送）
    '================================================
    Private Sub Com_Send()
        'シリアルポートをオープンしていない場合、処理を行わない.
        If SerialPort1.IsOpen = False Then
            Return
        End If

        'テキストボックスから、送信するテキストを取り出す.
        Dim data As String
        data = SndTextBox.Text

        '送信するテキストがない場合、データ送信は行わない.
        If String.IsNullOrEmpty(data) Then
            Return
        End If

        Try
            'シリアルポートからテキストを送信する.
            SerialPort1.WriteLine(data)

            '送信データを入力するテキストボックスをクリアする.
            SndTextBox.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    '================================================
    '   シリアル受信（未使用）
    '       buffer      受信データを格納するバイト型の配列。
    '       offset	    受信データの格納を開始する配列の位置。
    '       count	    受信データを読み込むバイト数。
    '================================================
    Private Sub Com_Recived(buffer As Byte(), offset As Integer, count As Integer)
        'シリアルポートをオープンしていない場合、処理を行わない.
        If SerialPort1.IsOpen = False Then
            Return
        End If

        Try
            '受信データを読み込む.
            Dim data As String
            'data = SerialPort1.ReadExisting()
            data = SerialPort1.ReadLine()

            '受信したデータをテキストボックスに書き込む.
            Me.Label27.Text = data

            'Dim args(0) As Object
            'args(0) = data
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    '================================================
    '   接続・切断　ボタン
    '================================================
    Private Sub ConnectButton_Click(sender As Object, e As EventArgs) Handles ConnectButton.Click

        If SerialPort1.IsOpen = True Then
            'もし接続中なら切断する

            'シリアルポートをクローズする.
            SerialPort1.Close()

            'ボタンの表示を[切断]から[接続]に変える.
            ConnectButton.Text = "接続"
            Label25.BackColor = Color.Gray
        Else
            'もし切断中なら接続する

            'オープンするシリアルポートをコンボボックスから取り出す.
            'SerialPort1.PortName = Form_SETTEI.T2.SelectedItem.ToString()
            SerialPort1.PortName = Label28.Text
            'デリミタとなる文字列（改行コード）を設定
            SerialPort1.NewLine = Chr(13)

            'データビットをセットする
            SerialPort1.DataBits = Form_SETTEI.T4.Text

            'パリティビットをセットする
            Select Case Form_SETTEI.T5.SelectedIndex
                Case 0
                    SerialPort1.Parity = Parity.None
                Case 1
                    SerialPort1.Parity = Parity.Odd
                Case 2
                    SerialPort1.Parity = Parity.Even
                Case 3
                    SerialPort1.Parity = Parity.Mark
                Case 4
                    SerialPort1.Parity = Parity.Space
            End Select

            'ストップビットをセットする. (ストップビット = 1ビット)
            Select Case Form_SETTEI.T6.SelectedIndex
                Case 0
                    SerialPort1.StopBits = StopBits.None
                Case 1
                    SerialPort1.StopBits = StopBits.One
                Case 2
                    SerialPort1.StopBits = StopBits.OnePointFive
                Case 3
                    SerialPort1.StopBits = StopBits.Two
            End Select

            '文字コードをセットする.
            SerialPort1.Encoding = Encoding.ASCII

            Try
                'シリアルポートをオープンする.
                SerialPort1.Open()

                'ボタンの表示を[接続]から[切断]に変える.
                ConnectButton.Text = "切断"
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Label25.BackColor = Color.Yellow

        End If


    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click

        If SerialPort1.IsOpen = False Then
            Return
        End If

        Button10.Text = "受信待ち中"

        ' Timer4を利用しない（デリゲート宣言したサブルーチンで処理する）
        ' Timer4.Enabled = True
        ' Timer4.Enabled = False

    End Sub

    Private Sub Timer4_Tick(sender As Object, e As EventArgs) Handles Timer4.Tick
        set_Panel() '3秒おきにJコマンドを送る
        If Label25.ForeColor <> Color.Gray Then
            Label25.ForeColor = Color.Gray
        Else
            Label25.ForeColor = Color.White
        End If
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
    End Sub

    Private Sub SerialPort1_DataReceived(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived

        'DisplayText()をデリゲート宣言
        Dim dlg As New DisplayTextDelegate(AddressOf DisplayText)
        'ポートオープン時に、SerialPort1.NewLine = Chr(13)で指定したデリミタ文字列（改行コード）までを受信
        Dim str As String = SerialPort1.ReadLine()
        '受信が完了したらデリゲート宣言されたサブルーチンを呼び出す
        Me.Invoke(dlg, New Object() {str})

    End Sub

    'Invokeメソッドで使用するデリゲート宣言  
    Delegate Sub DisplayTextDelegate(ByVal strDisp As String)


    '================================================
    '   シリアル受信
    '================================================
    Private Sub DisplayText(ByVal strDisp As String)
        Dim ySUM As Integer
        Dim xdat As String
        Dim xT1 As Integer
        Dim xT2 As Integer

        Log("受信生: " + strDisp)

        '受信したデータをテキストボックスに書き込む
        Me.Label27.Text = strDisp

        '電文チェック（ASCIIのみ）
        If Len(strDisp) Mod 2 = 1 Then
            'MsgBox("ASCII電文は偶数桁のはず「" + strDisp + "」", MessageBoxIcon.Error, "電文異常")
            Log("電文異常: ASCII電文は偶数桁のはず「" + strDisp + "」")
            Exit Sub
        End If

        'チェックサムを計算する
        ySUM = 0
        xdat = ""
        For i = 1 To Len(strDisp) - 2 Step 2
            xdat = Mid(strDisp, i, 2)
            ySUM = ySUM + Convert.ToInt32(xdat, 16) '16進数文字列を数値へ
        Next
        ySUM = ySUM Mod 256
        '電文最後のチェックサムと計算したチェックサムが合致しない場合
        If Mid(strDisp, Len(strDisp) - 1, 2) <> ySUM.ToString("X2") Then
            'NAKを送る
            Com_SendDenbun(Com_NAK)
            Exit Sub
        End If


        '"02ccNNxxyyzz03"
        '受信電文デストリビュータ
        Select Case Mid(strDisp, 3, 2) 'コマンドを取り出す
            Case "6d", "6D"     'm 起動通知（1:起動完了）
                Com_SendDenbun(Com_ACK) 'ACKを返す
                Label33.BackColor = Color.Yellow
                Timer4.Enabled = True

            Case "6e", "6E"     'n モード通知（0:TEST 1:RUN）
                Com_SendDenbun(Com_ACK) 'ACKを返す
                xxTESTRUN(Mid(strDisp, 7, 2))

            Case "6f", "6F"     'o RUN通知（0:実行終了 1:実行開始）
                Com_SendDenbun(Com_ACK) 'ACKを返す
                xxJIKKOU(Mid(strDisp, 7, 2))

            Case "06"           'ACK
                'ACKが来たフラグを立てる
                xAckBack = 1
                xSendCount = 0

            Case "15"           'NAK
                '直前の電文を再送
                Com_Send()
                xSendCount = xSendCount + 1
                If xSendCount > 10 Then
                    MsgBox("タイムカウントオーバー",, "ERROR")
                    xSendCount = 0
                End If


            Case "61" 'a
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T2.Text <> (xT1 / 10).ToString Then
                    T2.BackColor = Color.Pink
                Else
                    T2.BackColor = Color.White
                End If
                T2.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T10.Text <> (xT2 / 10).ToString Then
                    T10.BackColor = Color.Pink
                Else
                    T10.BackColor = Color.White
                End If
                T10.Text = (xT2 / 10).ToString

            Case "62" 'b
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T2.Text <> (xT1 / 10).ToString Then
                    T3.BackColor = Color.Pink
                Else
                    T3.BackColor = Color.White
                End If
                T3.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T11.Text <> (xT2 / 10).ToString Then
                    T11.BackColor = Color.Pink
                Else
                    T11.BackColor = Color.White
                End If
                T11.Text = (xT2 / 10).ToString

            Case "63" 'c
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T4.Text <> (xT1 / 10).ToString Then
                    T4.BackColor = Color.Pink
                Else
                    T4.BackColor = Color.White
                End If
                T4.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T12.Text <> (xT2 / 10).ToString Then
                    T12.BackColor = Color.Pink
                Else
                    T12.BackColor = Color.White
                End If
                T12.Text = (xT2 / 10).ToString

            Case "64" 'd
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T5.Text <> (xT1 / 10).ToString Then
                    T5.BackColor = Color.Pink
                Else
                    T5.BackColor = Color.White
                End If
                T5.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T13.Text <> (xT2 / 10).ToString Then
                    T13.BackColor = Color.Pink
                Else
                    T13.BackColor = Color.White
                End If
                T13.Text = (xT2 / 10).ToString

            Case "65" 'e
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T6.Text <> (xT1 / 10).ToString Then
                    T6.BackColor = Color.Pink
                Else
                    T6.BackColor = Color.White
                End If
                T6.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T14.Text <> (xT2 / 10).ToString Then
                    T14.BackColor = Color.Pink
                Else
                    T14.BackColor = Color.White
                End If
                T14.Text = (xT2 / 10).ToString

            Case "66" 'f
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T7.Text <> (xT1 / 10).ToString Then
                    T7.BackColor = Color.Pink
                Else
                    T7.BackColor = Color.White
                End If
                T7.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T15.Text <> (xT2 / 10).ToString Then
                    T15.BackColor = Color.Pink
                Else
                    T15.BackColor = Color.White
                End If
                T15.Text = (xT2 / 10).ToString

            Case "67" 'g
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T8.Text <> (xT1 / 10).ToString Then
                    T8.BackColor = Color.Pink
                Else
                    T8.BackColor = Color.White
                End If
                T8.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T16.Text <> (xT2 / 10).ToString Then
                    T16.BackColor = Color.Pink
                Else
                    T16.BackColor = Color.White
                End If
                T16.Text = (xT2 / 10).ToString

            Case "68" 'h
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If C1.Text <> (xT1 / 10).ToString Then
                    C1.BackColor = Color.Pink
                Else
                    C1.BackColor = Color.White
                End If
                C1.Text = xT1.ToString


            Case "75"   'u
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T1.Text <> (xT1 / 10).ToString Then
                    T1.BackColor = Color.Pink
                Else
                    T1.BackColor = Color.White
                End If
                T1.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T9.Text <> (xT2 / 10).ToString Then
                    T9.BackColor = Color.Pink
                Else
                    T9.BackColor = Color.White
                End If
                T9.Text = (xT2 / 10).ToString

            Case "76" 'v
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T100.Text <> (xT1 / 10).ToString Then
                    T100.BackColor = Color.Pink
                Else
                    T100.BackColor = Color.White
                End If
                T100.Text = (xT1 / 10).ToString


            Case "6B", "6b" 'k
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T102.Text <> (xT1 / 10).ToString Then
                    T102.BackColor = Color.Pink
                Else
                    T102.BackColor = Color.White
                End If
                T102.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T110.Text <> (xT2 / 10).ToString Then
                    T110.BackColor = Color.Pink
                Else
                    T110.BackColor = Color.White
                End If
                T110.Text = (xT2 / 10).ToString

            Case "6C", "6c" 'l
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T103.Text <> (xT1 / 10).ToString Then
                    T103.BackColor = Color.Pink
                Else
                    T103.BackColor = Color.White
                End If
                T103.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T111.Text <> (xT2 / 10).ToString Then
                    T111.BackColor = Color.Pink
                Else
                    T111.BackColor = Color.White
                End If
                T111.Text = (xT2 / 10).ToString

            Case "70" 'p
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T104.Text <> (xT1 / 10).ToString Then
                    T104.BackColor = Color.Pink
                Else
                    T104.BackColor = Color.White
                End If
                T104.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T112.Text <> (xT2 / 10).ToString Then
                    T112.BackColor = Color.Pink
                Else
                    T112.BackColor = Color.White
                End If
                T112.Text = (xT2 / 10).ToString

            Case "71" 'q
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T105.Text <> (xT1 / 10).ToString Then
                    T105.BackColor = Color.Pink
                Else
                    T105.BackColor = Color.White
                End If
                T105.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T113.Text <> (xT2 / 10).ToString Then
                    T113.BackColor = Color.Pink
                Else
                    T113.BackColor = Color.White
                End If
                T113.Text = (xT2 / 10).ToString

            Case "72" 'r
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T106.Text <> (xT1 / 10).ToString Then
                    T106.BackColor = Color.Pink
                Else
                    T106.BackColor = Color.White
                End If
                T106.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T114.Text <> (xT2 / 10).ToString Then
                    T114.BackColor = Color.Pink
                Else
                    T114.BackColor = Color.White
                End If
                T114.Text = (xT2 / 10).ToString

            Case "73" 's
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T107.Text <> (xT1 / 10).ToString Then
                    T107.BackColor = Color.Pink
                Else
                    T107.BackColor = Color.White
                End If
                T107.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T115.Text <> (xT2 / 10).ToString Then
                    T115.BackColor = Color.Pink
                Else
                    T115.BackColor = Color.White
                End If
                T115.Text = (xT2 / 10).ToString

            Case "74" 't
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T108.Text <> (xT1 / 10).ToString Then
                    T108.BackColor = Color.Pink
                Else
                    T108.BackColor = Color.White
                End If
                T108.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T116.Text <> (xT2 / 10).ToString Then
                    T116.BackColor = Color.Pink
                Else
                    T116.BackColor = Color.White
                End If
                T116.Text = (xT2 / 10).ToString

            Case "69" 'i
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If C2.Text <> (xT1 / 10).ToString Then
                    C2.BackColor = Color.Pink
                Else
                    C2.BackColor = Color.White
                End If
                C2.Text = xT1.ToString

            Case "77" 'w
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T200.Text <> (xT1 / 10).ToString Then
                    T200.BackColor = Color.Pink
                Else
                    T200.BackColor = Color.White
                End If
                T200.Text = (xT1 / 10).ToString

            Case "6A", "6a" 'j　ランプ状態
                set_Lump(Mid(strDisp, 7, 4), Mid(strDisp, 11, 2), Mid(strDisp, 13, 4))

        End Select

    End Sub



    '■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    'TESTモード、RUNモード切替
    Private Sub xxTESTRUN(xdat As String)
        Select Case xdat
            Case "00"
                'TESTモード
                PicTEST.Visible = True
                PicRUN.Visible = False
                Label76.BackColor = Color.Cyan
                Label45.BackColor = Color.Cyan
                Label78.BackColor = Color.Lime
                Label77.BackColor = Color.Yellow
                If Label_START.Text = "ON" Then
                    Timer1.Enabled = False
                    Timer2.Enabled = False
                    Label_START.BackColor = Color.Gray
                    Label_START.Text = "OFF"
                End If
                Timer3.Enabled = False
                Pic_EMMR1.Visible = False
                Pic_EMMR2.Visible = False

            Case "01"
                'RUNモード
                PicRUN.Visible = True
                PicTEST.Visible = False
                Label76.BackColor = Color.MidnightBlue
                Label45.BackColor = Color.MidnightBlue
                Label78.BackColor = Color.DarkGreen
                Label77.BackColor = Color.Gray
                Pic_EMMR1.Visible = False
                Pic_EMMR2.Visible = True
        End Select
    End Sub






    '■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    '実行開始、終了
    Private Sub xxJIKKOU(xdat As String)
        Select Case xdat
            Case "01" '実行開始
                Label_START.BackColor = Color.Yellow
                Label_START.Text = "ON"
                Timer1.Enabled = True
            Case "00" '実行終了
                Label_START.BackColor = Color.Gray
                Label_START.Text = "OFF"
                Timer1.Enabled = False
                Timer2.Enabled = False
        End Select

    End Sub






    '以下、デバッグ用　■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■



    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim strDisp As String = ""
        Dim ySUM As Integer
        Dim xdat As String
        Dim xT1 As Integer
        Dim xT2 As Integer

        xAckBack = 1
        Com_SendDenbun(TextBox2.Text)

        strDisp = SndTextBox.Text
        If strDisp = "" Then Exit Sub

        'チェックサムを計算する
        ySUM = 0
        xdat = ""
        For i = 1 To Len(strDisp) - 2 Step 2
            xdat = Mid(strDisp, i, 2)
            ySUM = ySUM + Convert.ToInt32(xdat, 16) '16進数文字列を数値へ
        Next
        ySUM = ySUM Mod 256
        '電文最後のチェックサムと計算したチェックサムが合致しない場合
        If Mid(strDisp, Len(strDisp) - 1, 2) <> ySUM.ToString("X2") Then
            'NAKを送る
            Com_SendDenbun(Com_NAK)
            Exit Sub
        End If

        '"02ccNNxxyyzz03"
        '受信電文デストリビュータ
        Select Case Mid(strDisp, 3, 2) 'コマンドを取り出す
            Case "6d", "6D"     'm 起動通知（1:起動完了）
                Com_SendDenbun(Com_ACK) 'ACKを返す
                Label33.BackColor = Color.Yellow
                Timer4.Enabled = True

            Case "6e", "6E"     'n モード通知（0:TEST 1:RUN）
                Com_SendDenbun(Com_ACK) 'ACKを返す
                xxTESTRUN(Mid(strDisp, 7, 2))

            Case "6f", "6F"     'o RUN通知（0:実行終了 1:実行開始）
                Com_SendDenbun(Com_ACK) 'ACKを返す
                xxJIKKOU(Mid(strDisp, 7, 2))

            Case "06"           'ACK
                'ACKが来たフラグを立てる
                xAckBack = 1
                xSendCount = 0

            Case "15"           'NAK
                '直前の電文を再送
                Com_Send()
                xSendCount = xSendCount + 1
                If xSendCount > 10 Then
                    MsgBox("タイムカウントオーバー",, "ERROR")
                    xSendCount = 0
                End If


            Case "61" 'a
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T2.Text <> (xT1 / 10).ToString Then
                    T2.BackColor = Color.Pink
                Else
                    T2.BackColor = Color.White
                End If
                T2.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T10.Text <> (xT2 / 10).ToString Then
                    T10.BackColor = Color.Pink
                Else
                    T10.BackColor = Color.White
                End If
                T10.Text = (xT2 / 10).ToString

            Case "62" 'b
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T2.Text <> (xT1 / 10).ToString Then
                    T3.BackColor = Color.Pink
                Else
                    T3.BackColor = Color.White
                End If
                T3.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T11.Text <> (xT2 / 10).ToString Then
                    T11.BackColor = Color.Pink
                Else
                    T11.BackColor = Color.White
                End If
                T11.Text = (xT2 / 10).ToString

            Case "63" 'c
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T4.Text <> (xT1 / 10).ToString Then
                    T4.BackColor = Color.Pink
                Else
                    T4.BackColor = Color.White
                End If
                T4.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T12.Text <> (xT2 / 10).ToString Then
                    T12.BackColor = Color.Pink
                Else
                    T12.BackColor = Color.White
                End If
                T12.Text = (xT2 / 10).ToString

            Case "64" 'd
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T5.Text <> (xT1 / 10).ToString Then
                    T5.BackColor = Color.Pink
                Else
                    T5.BackColor = Color.White
                End If
                T5.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T13.Text <> (xT2 / 10).ToString Then
                    T13.BackColor = Color.Pink
                Else
                    T13.BackColor = Color.White
                End If
                T13.Text = (xT2 / 10).ToString

            Case "65" 'e
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T6.Text <> (xT1 / 10).ToString Then
                    T6.BackColor = Color.Pink
                Else
                    T6.BackColor = Color.White
                End If
                T6.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T14.Text <> (xT2 / 10).ToString Then
                    T14.BackColor = Color.Pink
                Else
                    T14.BackColor = Color.White
                End If
                T14.Text = (xT2 / 10).ToString

            Case "66" 'f
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T7.Text <> (xT1 / 10).ToString Then
                    T7.BackColor = Color.Pink
                Else
                    T7.BackColor = Color.White
                End If
                T7.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T15.Text <> (xT2 / 10).ToString Then
                    T15.BackColor = Color.Pink
                Else
                    T15.BackColor = Color.White
                End If
                T15.Text = (xT2 / 10).ToString

            Case "67" 'g
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T8.Text <> (xT1 / 10).ToString Then
                    T8.BackColor = Color.Pink
                Else
                    T8.BackColor = Color.White
                End If
                T8.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T16.Text <> (xT2 / 10).ToString Then
                    T16.BackColor = Color.Pink
                Else
                    T16.BackColor = Color.White
                End If
                T16.Text = (xT2 / 10).ToString

            Case "68" 'h
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If C1.Text <> (xT1 / 10).ToString Then
                    C1.BackColor = Color.Pink
                Else
                    C1.BackColor = Color.White
                End If
                C1.Text = xT1.ToString


            Case "75"   'u
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T1.Text <> (xT1 / 10).ToString Then
                    T1.BackColor = Color.Pink
                Else
                    T1.BackColor = Color.White
                End If
                T1.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T9.Text <> (xT2 / 10).ToString Then
                    T9.BackColor = Color.Pink
                Else
                    T9.BackColor = Color.White
                End If
                T9.Text = (xT2 / 10).ToString

            Case "76" 'v
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T100.Text <> (xT1 / 10).ToString Then
                    T100.BackColor = Color.Pink
                Else
                    T100.BackColor = Color.White
                End If
                T100.Text = (xT1 / 10).ToString


            Case "6B", "6b" 'k
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T102.Text <> (xT1 / 10).ToString Then
                    T102.BackColor = Color.Pink
                Else
                    T102.BackColor = Color.White
                End If
                T102.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T110.Text <> (xT2 / 10).ToString Then
                    T110.BackColor = Color.Pink
                Else
                    T110.BackColor = Color.White
                End If
                T110.Text = (xT2 / 10).ToString

            Case "6C", "6c" 'l
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T103.Text <> (xT1 / 10).ToString Then
                    T103.BackColor = Color.Pink
                Else
                    T103.BackColor = Color.White
                End If
                T103.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T111.Text <> (xT2 / 10).ToString Then
                    T111.BackColor = Color.Pink
                Else
                    T111.BackColor = Color.White
                End If
                T111.Text = (xT2 / 10).ToString

            Case "70" 'p
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T104.Text <> (xT1 / 10).ToString Then
                    T104.BackColor = Color.Pink
                Else
                    T104.BackColor = Color.White
                End If
                T104.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T112.Text <> (xT2 / 10).ToString Then
                    T112.BackColor = Color.Pink
                Else
                    T112.BackColor = Color.White
                End If
                T112.Text = (xT2 / 10).ToString

            Case "71" 'q
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T105.Text <> (xT1 / 10).ToString Then
                    T105.BackColor = Color.Pink
                Else
                    T105.BackColor = Color.White
                End If
                T105.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T113.Text <> (xT2 / 10).ToString Then
                    T113.BackColor = Color.Pink
                Else
                    T113.BackColor = Color.White
                End If
                T113.Text = (xT2 / 10).ToString

            Case "72" 'r
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T106.Text <> (xT1 / 10).ToString Then
                    T106.BackColor = Color.Pink
                Else
                    T106.BackColor = Color.White
                End If
                T106.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T114.Text <> (xT2 / 10).ToString Then
                    T114.BackColor = Color.Pink
                Else
                    T114.BackColor = Color.White
                End If
                T114.Text = (xT2 / 10).ToString

            Case "73" 's
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T107.Text <> (xT1 / 10).ToString Then
                    T107.BackColor = Color.Pink
                Else
                    T107.BackColor = Color.White
                End If
                T107.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T115.Text <> (xT2 / 10).ToString Then
                    T115.BackColor = Color.Pink
                Else
                    T115.BackColor = Color.White
                End If
                T115.Text = (xT2 / 10).ToString

            Case "74" 't
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T108.Text <> (xT1 / 10).ToString Then
                    T108.BackColor = Color.Pink
                Else
                    T108.BackColor = Color.White
                End If
                T108.Text = (xT1 / 10).ToString
                xT2 = Convert.ToInt32(Mid(strDisp, 11, 4), 16) '16進数文字列を数値へ
                If T116.Text <> (xT2 / 10).ToString Then
                    T116.BackColor = Color.Pink
                Else
                    T116.BackColor = Color.White
                End If
                T116.Text = (xT2 / 10).ToString

            Case "69" 'i
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If C2.Text <> (xT1 / 10).ToString Then
                    C2.BackColor = Color.Pink
                Else
                    C2.BackColor = Color.White
                End If
                C2.Text = xT1.ToString

            Case "77" 'w
                xT1 = Convert.ToInt32(Mid(strDisp, 7, 4), 16) '16進数文字列を数値へ
                If T200.Text <> (xT1 / 10).ToString Then
                    T200.BackColor = Color.Pink
                Else
                    T200.BackColor = Color.White
                End If
                T200.Text = (xT1 / 10).ToString

            Case "6A", "6a" 'j　ランプ状態
                set_Lump(Mid(strDisp, 7, 4), Mid(strDisp, 11, 2), Mid(strDisp, 13, 4))

        End Select


    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        xAckBack = 1
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        For i = 1 To 100 Step 2
            Log(i)
        Next
    End Sub

    Private Sub Log(msg As String)
        ListBox3.Items.Add(msg)
        ListBox3.TopIndex = ListBox3.Items.Count - 1
    End Sub
End Class