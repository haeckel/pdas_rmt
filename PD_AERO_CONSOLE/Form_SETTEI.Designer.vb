﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_SETTEI
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.T18 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.T17 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.T16 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.T15 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.T14 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.T10 = New System.Windows.Forms.TextBox()
        Me.T13 = New System.Windows.Forms.ComboBox()
        Me.T12 = New System.Windows.Forms.ComboBox()
        Me.T11 = New System.Windows.Forms.ComboBox()
        Me.T9 = New System.Windows.Forms.ComboBox()
        Me.T8 = New System.Windows.Forms.ComboBox()
        Me.T1 = New System.Windows.Forms.TextBox()
        Me.T2 = New System.Windows.Forms.ComboBox()
        Me.T7 = New System.Windows.Forms.ComboBox()
        Me.T6 = New System.Windows.Forms.ComboBox()
        Me.T5 = New System.Windows.Forms.ComboBox()
        Me.T4 = New System.Windows.Forms.ComboBox()
        Me.T3 = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.T18)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.T17)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.T16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.T15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.T14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.T10)
        Me.GroupBox1.Controls.Add(Me.T13)
        Me.GroupBox1.Controls.Add(Me.T12)
        Me.GroupBox1.Controls.Add(Me.T11)
        Me.GroupBox1.Controls.Add(Me.T9)
        Me.GroupBox1.Controls.Add(Me.T8)
        Me.GroupBox1.Controls.Add(Me.T1)
        Me.GroupBox1.Controls.Add(Me.T2)
        Me.GroupBox1.Controls.Add(Me.T7)
        Me.GroupBox1.Controls.Add(Me.T6)
        Me.GroupBox1.Controls.Add(Me.T5)
        Me.GroupBox1.Controls.Add(Me.T4)
        Me.GroupBox1.Controls.Add(Me.T3)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.Label37)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkGreen
        Me.GroupBox1.Location = New System.Drawing.Point(6, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(808, 416)
        Me.GroupBox1.TabIndex = 187
        Me.GroupBox1.TabStop = False
        '
        'T18
        '
        Me.T18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T18.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T18.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T18.Location = New System.Drawing.Point(527, 285)
        Me.T18.MaxLength = 0
        Me.T18.Name = "T18"
        Me.T18.Size = New System.Drawing.Size(127, 23)
        Me.T18.TabIndex = 388
        Me.T18.Text = "2000"
        Me.T18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Thistle
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(527, 252)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(127, 35)
        Me.Label16.TabIndex = 387
        Me.Label16.Text = "カウンタ値ワーニング（上限）"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'T17
        '
        Me.T17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T17.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T17.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T17.Location = New System.Drawing.Point(394, 285)
        Me.T17.MaxLength = 0
        Me.T17.Name = "T17"
        Me.T17.Size = New System.Drawing.Size(127, 23)
        Me.T17.TabIndex = 386
        Me.T17.Text = "4"
        Me.T17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Thistle
        Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(394, 252)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(127, 35)
        Me.Label17.TabIndex = 385
        Me.Label17.Text = "カウンタ値ワーニング（下限）"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'T16
        '
        Me.T16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T16.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T16.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T16.Location = New System.Drawing.Point(261, 285)
        Me.T16.MaxLength = 0
        Me.T16.Name = "T16"
        Me.T16.Size = New System.Drawing.Size(127, 23)
        Me.T16.TabIndex = 384
        Me.T16.Text = "200"
        Me.T16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Thistle
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(261, 252)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(127, 35)
        Me.Label15.TabIndex = 383
        Me.Label15.Text = "タイマー値ワーニング（上限）"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'T15
        '
        Me.T15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T15.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T15.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T15.Location = New System.Drawing.Point(128, 285)
        Me.T15.MaxLength = 0
        Me.T15.Name = "T15"
        Me.T15.Size = New System.Drawing.Size(127, 23)
        Me.T15.TabIndex = 382
        Me.T15.Text = "0.4"
        Me.T15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Thistle
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(128, 252)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(127, 35)
        Me.Label14.TabIndex = 381
        Me.Label14.Text = "タイマー値ワーニング（下限）"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.LightSeaGreen
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(21, 342)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(69, 52)
        Me.Button1.TabIndex = 380
        Me.Button1.Text = "設定保存"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'T14
        '
        Me.T14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T14.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T14.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T14.Location = New System.Drawing.Point(17, 285)
        Me.T14.MaxLength = 0
        Me.T14.Name = "T14"
        Me.T14.Size = New System.Drawing.Size(105, 23)
        Me.T14.TabIndex = 377
        Me.T14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Thistle
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(17, 252)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(105, 35)
        Me.Label13.TabIndex = 376
        Me.Label13.Text = "パネル読込間隔（ミリ秒）"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'T10
        '
        Me.T10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T10.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T10.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T10.Location = New System.Drawing.Point(352, 201)
        Me.T10.MaxLength = 0
        Me.T10.Name = "T10"
        Me.T10.Size = New System.Drawing.Size(105, 23)
        Me.T10.TabIndex = 367
        Me.T10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'T13
        '
        Me.T13.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T13.FormattingEnabled = True
        Me.T13.Items.AddRange(New Object() {"0（なし）", "1（ETX）", "2（CR）", "3（CR+LF）"})
        Me.T13.Location = New System.Drawing.Point(685, 200)
        Me.T13.Name = "T13"
        Me.T13.Size = New System.Drawing.Size(108, 24)
        Me.T13.TabIndex = 374
        Me.T13.Text = "3（CR+LF）"
        '
        'T12
        '
        Me.T12.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T12.FormattingEnabled = True
        Me.T12.Items.AddRange(New Object() {"0（なし）", "1（STX）"})
        Me.T12.Location = New System.Drawing.Point(588, 200)
        Me.T12.Name = "T12"
        Me.T12.Size = New System.Drawing.Size(91, 24)
        Me.T12.TabIndex = 370
        Me.T12.Text = "1（STX）"
        '
        'T11
        '
        Me.T11.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T11.FormattingEnabled = True
        Me.T11.Items.AddRange(New Object() {"0（テキスト）", "1（バイナリ） "})
        Me.T11.Location = New System.Drawing.Point(463, 200)
        Me.T11.Name = "T11"
        Me.T11.Size = New System.Drawing.Size(119, 24)
        Me.T11.TabIndex = 368
        Me.T11.Text = "0（テキスト）"
        '
        'T9
        '
        Me.T9.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T9.FormattingEnabled = True
        Me.T9.Items.AddRange(New Object() {"0（送信完了を待たない）", "1（送信完了を待つ）"})
        Me.T9.Location = New System.Drawing.Point(144, 200)
        Me.T9.Name = "T9"
        Me.T9.Size = New System.Drawing.Size(202, 24)
        Me.T9.TabIndex = 364
        Me.T9.Text = "0（送信完了を待たない）"
        '
        'T8
        '
        Me.T8.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T8.FormattingEnabled = True
        Me.T8.Items.AddRange(New Object() {"0（イベント）", "1（ポーリング）"})
        Me.T8.Location = New System.Drawing.Point(17, 200)
        Me.T8.Name = "T8"
        Me.T8.Size = New System.Drawing.Size(121, 24)
        Me.T8.TabIndex = 362
        Me.T8.Text = "1（ポーリング）"
        '
        'T1
        '
        Me.T1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T1.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T1.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.T1.Location = New System.Drawing.Point(17, 116)
        Me.T1.MaxLength = 0
        Me.T1.Name = "T1"
        Me.T1.Size = New System.Drawing.Size(73, 23)
        Me.T1.TabIndex = 3
        Me.T1.Text = "1022"
        Me.T1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'T2
        '
        Me.T2.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T2.FormattingEnabled = True
        Me.T2.Location = New System.Drawing.Point(96, 115)
        Me.T2.Name = "T2"
        Me.T2.Size = New System.Drawing.Size(78, 24)
        Me.T2.TabIndex = 372
        Me.T2.Text = "COM1"
        '
        'T7
        '
        Me.T7.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T7.FormattingEnabled = True
        Me.T7.Items.AddRange(New Object() {"0（なし）", "1（DTR・DSR）", "2（RTS・CTS）", "4（Xon・Xoff） "})
        Me.T7.Location = New System.Drawing.Point(644, 115)
        Me.T7.Name = "T7"
        Me.T7.Size = New System.Drawing.Size(149, 24)
        Me.T7.TabIndex = 360
        Me.T7.Text = "0（なし）"
        '
        'T6
        '
        Me.T6.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T6.FormattingEnabled = True
        Me.T6.Items.AddRange(New Object() {"0（なし）", "1（１ビット）", "2（1.5ビット）", "3（２ビット）"})
        Me.T6.Location = New System.Drawing.Point(520, 115)
        Me.T6.Name = "T6"
        Me.T6.Size = New System.Drawing.Size(118, 24)
        Me.T6.TabIndex = 358
        Me.T6.Text = "2（２ビット）"
        '
        'T5
        '
        Me.T5.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T5.FormattingEnabled = True
        Me.T5.Items.AddRange(New Object() {"0（ノンパリティ）", "1（奇数パリティ）", "2（偶数パリティ）", "3（マーク）", "4（スペース）"})
        Me.T5.Location = New System.Drawing.Point(352, 115)
        Me.T5.Name = "T5"
        Me.T5.Size = New System.Drawing.Size(162, 24)
        Me.T5.TabIndex = 356
        Me.T5.Text = "0（ノンパリティ）"
        '
        'T4
        '
        Me.T4.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T4.FormattingEnabled = True
        Me.T4.Items.AddRange(New Object() {"8", "7", "6", "5"})
        Me.T4.Location = New System.Drawing.Point(287, 115)
        Me.T4.Name = "T4"
        Me.T4.Size = New System.Drawing.Size(59, 24)
        Me.T4.TabIndex = 354
        Me.T4.Text = "8"
        '
        'T3
        '
        Me.T3.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.T3.FormattingEnabled = True
        Me.T3.Items.AddRange(New Object() {"256000", "128000", "115200", "57600", "56000", "38400", "19200", "14400", "9600", "4800", "2400", "1200", "600", "300", "110"})
        Me.T3.Location = New System.Drawing.Point(180, 115)
        Me.T3.Name = "T3"
        Me.T3.Size = New System.Drawing.Size(101, 24)
        Me.T3.TabIndex = 352
        Me.T3.Text = "38400"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.YellowGreen
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(685, 168)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(108, 35)
        Me.Label12.TabIndex = 375
        Me.Label12.Text = "ターミネイタ"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(96, 93)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 25)
        Me.Label11.TabIndex = 373
        Me.Label11.Text = "COM"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.YellowGreen
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(588, 168)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(91, 35)
        Me.Label10.TabIndex = 371
        Me.Label10.Text = "ヘッダ"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.YellowGreen
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(463, 168)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(119, 35)
        Me.Label9.TabIndex = 369
        Me.Label9.Text = "バイナリモード "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.YellowGreen
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(352, 168)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(105, 35)
        Me.Label8.TabIndex = 366
        Me.Label8.Text = "送信タイムアウト時間（ミリ秒）"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.YellowGreen
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(144, 168)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(202, 35)
        Me.Label7.TabIndex = 365
        Me.Label7.Text = "送信モード"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.YellowGreen
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(17, 168)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 35)
        Me.Label6.TabIndex = 363
        Me.Label6.Text = "受信モード"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(644, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(149, 25)
        Me.Label5.TabIndex = 361
        Me.Label5.Text = "フロー制御"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(520, 93)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 25)
        Me.Label4.TabIndex = 359
        Me.Label4.Text = "ストップビット"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(352, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(162, 25)
        Me.Label3.TabIndex = 357
        Me.Label3.Text = "パリティ"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(287, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 25)
        Me.Label2.TabIndex = 355
        Me.Label2.Text = "ビット"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Black
        Me.Label30.Location = New System.Drawing.Point(180, 93)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(101, 25)
        Me.Label30.TabIndex = 353
        Me.Label30.Text = "ボーレート"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(723, 342)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(69, 52)
        Me.Button7.TabIndex = 321
        Me.Button7.Text = "戻る"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.Black
        Me.Label37.Location = New System.Drawing.Point(17, 93)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(73, 24)
        Me.Label37.TabIndex = 187
        Me.Label37.Text = "ポート"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.PaleGreen
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Font = New System.Drawing.Font("MS UI Gothic", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Green
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(776, 42)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "通信その他設定"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form_SETTEI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(820, 421)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form_SETTEI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "通信設定"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents T1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label30 As Label
    Friend WithEvents T3 As ComboBox
    Friend WithEvents T5 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents T4 As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents T7 As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents T6 As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents T8 As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents T11 As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents T10 As TextBox
    Friend WithEvents T9 As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents T12 As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents T13 As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents T2 As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents T14 As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents T15 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents T18 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents T17 As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents T16 As TextBox
    Friend WithEvents Label15 As Label
End Class
