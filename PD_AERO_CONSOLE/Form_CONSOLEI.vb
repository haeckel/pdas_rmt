﻿'=============================================================================================
'   
'
'
'   宇宙の仕事
'
'
'
'=============================================================================================
Imports System.IO
Imports System.IO.Ports
Imports System.Text


Public Class Form_CONSOLE

    Dim xx_Panel As Integer

    'システム異常
    Const Err_S_Watchdog As String = "E001"
    Const Err_S_24Volt As String = "E002"
    Const Err_S_6Volt As String = "E003"
    Const Err_S_EMGcut As String = "E004"
    'エンジン異常
    Const Err_E_Gas As String = "E101"
    Const Err_E_Fire As String = "E102"
    Const Err_E_Klow As String = "E103"
    Const Err_E_OverH1 As String = "E111"
    Const Err_E_OverH2 As String = "E112"
    Const Err_E_OverH3 As String = "E113"
    Const Err_E_OverH4 As String = "E114"
    Const Err_E_OverH5 As String = "E115"
    '通信コマンド異常1
    Const A_Com1_CSum As String = "E201"
    Const A_Com1_CRet As String = "E202"
    Const B_Com1_CSum As String = "E211"
    Const B_Com1_CRet As String = "E212"
    Const C_Com1_CSum As String = "E221"
    Const C_Com1_CRet As String = "E222"
    Const D_Com1_CSum As String = "E231"
    Const D_Com1_CRet As String = "E232"
    Const E_Com1_CSum As String = "E241"
    Const E_Com1_CRet As String = "E242"
    Const F_Com1_CSum As String = "E251"
    Const F_Com1_CRet As String = "E252"
    Const G_Com1_CSum As String = "E261"
    Const G_Com1_CRet As String = "E262"
    Const H_Com1_CSum As String = "E271"
    Const H_Com1_CRet As String = "E272"
    Const I_Com1_CSum As String = "E281"
    Const I_Com1_CRet As String = "E282"
    Const J_Com1_CSum As String = "E291"
    Const J_Com1_CRet As String = "E292"
    '通信コマンド異常2
    Const a_Com2_CSum As String = "E301"
    Const a_Com2_CRet As String = "E302"
    Const b_Com2_CSum As String = "E311"
    Const b_Com2_CRet As String = "E312"
    Const c_Com2_CSum As String = "E321"
    Const c_Com2_CRet As String = "E322"
    Const d_Com2_CSum As String = "E331"
    Const d_Com2_CRet As String = "E332"
    Const e_Com2_CSum As String = "E341"
    Const e_Com2_CRet As String = "E342"
    Const f_Com2_CSum As String = "E351"
    Const f_Com2_CRet As String = "E352"
    Const g_Com2_CSum As String = "E361"
    Const g_Com2_CRet As String = "E362"
    Const h_Com2_CSum As String = "E371"
    Const h_Com2_CRet As String = "E372"
    Const i_Com2_CSum As String = "E381"
    Const i_Com2_CRet As String = "E382"
    Const j_Com2_CSum As String = "E391"
    Const j_Com2_CRet As String = "E392"
    Const k_Com2_CSum As String = "E391"
    Const k_Com2_CRet As String = "E392"
    Const l_Com2_CSum As String = "E391"
    Const l_Com2_CRet As String = "E392"
    Const m_Com2_CSum As String = "E391"
    Const m_Com2_CRet As String = "E392"
    Const n_Com2_CSum As String = "E391"
    Const n_Com2_CRet As String = "E392"
    Const o_Com2_CSum As String = "E391"
    Const o_Com2_CRet As String = "E392"
    'RUNシーケンス異常
    Const o_EMR_Stop As String = "E401"

    '通信コマンド
    Const Com_STX As Byte = &H2
    Const Com_ETX As Byte = &H3
    Const Com_CR As Byte = &HD


    '================================================
    '   通信クラス
    '================================================
    Private Class BuadRateItem
        Inherits Object

        Private m_name As String = ""
        Private m_value As Integer = 0

        '表示名称
        Public Property NAME() As String
            Set(ByVal value As String)
                m_name = value
            End Set
            Get
                Return m_name
            End Get
        End Property

        'ボーレート設定値.
        Public Property BAUDRATE() As Integer
            Set(ByVal value As Integer)
                m_value = value
            End Set
            Get
                Return m_value
            End Get
        End Property

        'コンボボックス表示用の文字列取得関数.
        Public Overrides Function ToString() As String
            Return m_name
        End Function

    End Class

    Private Class HandShakeItem
        Inherits Object

        Private m_name As String = ""
        Private m_value As Handshake = Handshake.None

        '表示名称
        Public Property NAME() As String
            Set(ByVal value As String)
                m_name = value
            End Set
            Get
                Return m_name
            End Get
        End Property

        '制御プロトコル設定値.
        Public Property HANDSHAKE() As Handshake
            Set(ByVal value As Handshake)
                m_value = value
            End Set
            Get
                Return m_value
            End Get
        End Property

        'コンボボックス表示用の文字列取得関数.
        Public Overrides Function ToString() As String
            Return m_name
        End Function

    End Class


    Dim a1 As Double
    Dim a2 As Double
    Dim a3 As Double
    Dim a4 As Double
    Public Property FormJyugyo As Object

    '================================================
    '   初期化
    '================================================
    Private Sub FormJyugyo_Choice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim fileno As Integer
        Dim xdat2 As String
        Dim arr() As String


        '保存パラメータ読み出し
        GetFolderName()

        '初期はテストモードにする
        PicRUN_Click(sender, e)

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            Form_SETTEI.T1.Text = arr(0)
            Label28.Text = arr(1)
            For i = 2 To 9
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            T10.Text = arr(9)
            For i = 11 To 13
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            For i = 14 To 18
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), TextBox).Text = arr(i - 1)
            Next
        End If

    End Sub


    '================================================
    '   アクティブ
    '================================================
    Private Sub FormJyugyo_Choice_Activated(sender As Object, e As EventArgs) Handles Me.Activated

        Dim fileno As Integer
        Dim xdat2 As String
        Dim arr() As String

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            Form_SETTEI.T1.Text = arr(0)
            Label28.Text = arr(1)
            For i = 2 To 9
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            T10.Text = arr(9)
            For i = 11 To 13
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), ComboBox).Text = arr(i - 1)
            Next
            For i = 14 To 18
                CType(Form_SETTEI.GroupBox1.Controls("T" & i), TextBox).Text = arr(i - 1)
            Next
        End If

    End Sub


    '================================================
    '   保存パラメータ読み出し
    '================================================
    Private Sub GetFolderName()
        Dim xdat As String
        Dim ydat As String
        Dim arr() As String
        Dim fileno As Integer
        Dim xdat2 As String

        ListBox1.Items.Clear()
        ListBox2.Items.Clear()

        Dim files As String() = System.IO.Directory.GetFiles(Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\")), "*.csv", System.IO.SearchOption.AllDirectories)

        ListBox2.Items.AddRange(files)

        If ListBox2.Items.Count < 1 Then Exit Sub

        For i = 0 To ListBox2.Items.Count - 1
            xdat = ListBox2.Items(i)
            ydat = Mid(xdat, InStrRev(xdat, "\") + 1, 10) + "   "
            Using read = New System.IO.StreamReader(xdat, System.Text.Encoding.Default, False)
                Dim strTemp As String = ""
                Dim strWrite As String = ""
                strTemp = read.ReadLine()
                strTemp = read.ReadLine()
                arr = strTemp.Split(",")
                ydat = ydat + arr(30) + Mid("                                   ", 1, 34 - Len(arr(30))) + arr(31)
                ListBox1.Items.Add(ydat)
                read.Close()
            End Using
        Next

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
            For i = 1 To 14
                CType(Me.Controls("L" & i), Label).Text = arr(14) + "～" + arr(15)
            Next
            L15.Text = "カウンタ " + arr(16) + "～" + arr(17)
            L16.Text = "カウンタ " + arr(16) + "～" + arr(17)
        Else
            For i = 1 To 16
                CType(Me.Controls("L" & i), Label).Text = ""
            Next
        End If

    End Sub


    '================================================
    '   戻る
    '================================================
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        'メッセージボックスを表示する 
        Dim result As DialogResult = MessageBox.Show("本当に終了してよろしいですか？",
                                             "確認",
                                             MessageBoxButtons.YesNoCancel,
                                             MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button2)

        '何が選択されたか調べる 
        If result = DialogResult.Yes Then
            '「はい」が選択された時 
            End
        ElseIf result = DialogResult.No Then
            '「いいえ」が選択された時 
            Exit Sub
        ElseIf result = DialogResult.Cancel Then
            '「キャンセル」が選択された時 
            Exit Sub
        End If
    End Sub


    '================================================
    '   パラメータ保存
    '================================================
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim path As String = ""
        Dim fileno As Integer
        Dim xdat As String

        If TextBox31.Text = "" Or TextBox1.Text = "" Then
            MsgBox("ファイル名とコメントを入力してください", MessageBoxIcon.Warning, "警告")
            Exit Sub
        End If

        'ダイアログを表示する
        path = Format(Now, "yyyy_MM_dd_") + TextBox31.Text + ".csv"
        'OKボタンがクリックされたとき、選択されたファイル名を表示する

        fileno = FreeFile()
        FileOpen(fileno, path, OpenMode.Output)
        xdat = "T2,T3,T4,T5,T6,T7,T8,T10,T11,T12,T13,T14,T15,T16,C1,T102,T103,T104,T105,T106,T107,T108,T110,T111,T112,T113,T114,T115,T116,C2,ファイル名,コメント"
        PrintLine(fileno, xdat)
        xdat = T2.Text
        For i = 3 To 8
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        For i = 10 To 16
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        xdat = xdat + "," + C1.Text
        For i = 102 To 108
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        For i = 110 To 116
            xdat = xdat + "," + CType(Me.Controls("T" & i), TextBox).Text
        Next
        xdat = xdat + "," + C2.Text
        xdat = xdat + "," + TextBox31.Text + "," + TextBox1.Text
        PrintLine(fileno, xdat)
        FileClose(fileno)

        MsgBox("ファイルを保存しました", MessageBoxIcon.Information, "完了")
        GetFolderName()

    End Sub


    '================================================
    '   設定画面へ
    '================================================
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Form_SETTEI.ShowDialog()

    End Sub


    '================================================
    '   パラメータ読み込みボタン
    '================================================
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim a As Integer
        Dim xdat As String
        Dim ydat As String
        Dim arr() As String

        If ListBox1.SelectedIndex < 0 Then Exit Sub

        a = ListBox1.SelectedIndex

        xdat = ListBox2.Items(a)
        ydat = Mid(xdat, InStrRev(xdat, "\") + 1, 10) + "   "
        Using read = New System.IO.StreamReader(xdat, System.Text.Encoding.Default, False)
            Dim strTemp As String = ""
            Dim strWrite As String = ""
            strTemp = read.ReadLine()
            strTemp = read.ReadLine()
            arr = strTemp.Split(",")
            read.Close()
        End Using

        TextBox31.Text = arr(30)
        TextBox1.Text = arr(31)
        For i = 2 To 8
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 2)
        Next
        For i = 10 To 16
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 3)
        Next
        C1.Text = arr(14)
        For i = 102 To 108
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 87)
        Next
        For i = 110 To 116
            CType(Me.Controls("T" & i), TextBox).Text = arr(i - 88)
        Next
        C2.Text = arr(29)

        Para_Check()

    End Sub


    '================================================
    '   パラメータリストのダブルクリック
    '================================================
    Private Sub ListBox1_DoubleClick(sender As Object, e As EventArgs) Handles ListBox1.DoubleClick
        'パラメータ読み込み処理へ
        Button3_Click(sender, e)
    End Sub


    '================================================
    '   入力欄のENTERキー移動処理
    '================================================
    Private Sub T2_KeyUp(sender As Object, e As KeyEventArgs) Handles T2.KeyUp
        If e.KeyCode = Keys.Enter Then T10.Focus()
    End Sub
    Private Sub T10_KeyUp(sender As Object, e As KeyEventArgs) Handles T10.KeyUp
        If e.KeyCode = Keys.Enter Then T3.Focus()
    End Sub
    Private Sub T3_KeyUp(sender As Object, e As KeyEventArgs) Handles T3.KeyUp
        If e.KeyCode = Keys.Enter Then T11.Focus()
    End Sub
    Private Sub T11_KeyUp(sender As Object, e As KeyEventArgs) Handles T11.KeyUp
        If e.KeyCode = Keys.Enter Then T4.Focus()
    End Sub
    Private Sub T4_KeyUp(sender As Object, e As KeyEventArgs) Handles T4.KeyUp
        If e.KeyCode = Keys.Enter Then T12.Focus()
    End Sub
    Private Sub T12_KeyUp(sender As Object, e As KeyEventArgs) Handles T12.KeyUp
        If e.KeyCode = Keys.Enter Then T5.Focus()
    End Sub
    Private Sub T5_KeyUp(sender As Object, e As KeyEventArgs) Handles T5.KeyUp
        If e.KeyCode = Keys.Enter Then T13.Focus()
    End Sub
    Private Sub T13_KeyUp(sender As Object, e As KeyEventArgs) Handles T13.KeyUp
        If e.KeyCode = Keys.Enter Then T6.Focus()
    End Sub
    Private Sub T6_KeyUp(sender As Object, e As KeyEventArgs) Handles T6.KeyUp
        If e.KeyCode = Keys.Enter Then T14.Focus()
    End Sub
    Private Sub T14_KeyUp(sender As Object, e As KeyEventArgs) Handles T14.KeyUp
        If e.KeyCode = Keys.Enter Then T7.Focus()
    End Sub
    Private Sub T7_KeyUp(sender As Object, e As KeyEventArgs) Handles T7.KeyUp
        If e.KeyCode = Keys.Enter Then T15.Focus()
    End Sub
    Private Sub T15_KeyUp(sender As Object, e As KeyEventArgs) Handles T15.KeyUp
        If e.KeyCode = Keys.Enter Then T8.Focus()
    End Sub
    Private Sub T8_KeyUp(sender As Object, e As KeyEventArgs) Handles T8.KeyUp
        If e.KeyCode = Keys.Enter Then T16.Focus()
    End Sub
    Private Sub T16_KeyUp(sender As Object, e As KeyEventArgs) Handles T16.KeyUp
        If e.KeyCode = Keys.Enter Then C1.Focus()
    End Sub
    Private Sub C1_KeyUp(sender As Object, e As KeyEventArgs) Handles C1.KeyUp
        If e.KeyCode = Keys.Enter Then T102.Focus()
    End Sub

    Private Sub T102_KeyUp(sender As Object, e As KeyEventArgs) Handles T102.KeyUp
        If e.KeyCode = Keys.Enter Then T110.Focus()
    End Sub
    Private Sub T110_KeyUp(sender As Object, e As KeyEventArgs) Handles T110.KeyUp
        If e.KeyCode = Keys.Enter Then T103.Focus()
    End Sub
    Private Sub T103_KeyUp(sender As Object, e As KeyEventArgs) Handles T103.KeyUp
        If e.KeyCode = Keys.Enter Then T111.Focus()
    End Sub
    Private Sub T111_KeyUp(sender As Object, e As KeyEventArgs) Handles T111.KeyUp
        If e.KeyCode = Keys.Enter Then T104.Focus()
    End Sub
    Private Sub T104_KeyUp(sender As Object, e As KeyEventArgs) Handles T104.KeyUp
        If e.KeyCode = Keys.Enter Then T112.Focus()
    End Sub
    Private Sub T112_KeyUp(sender As Object, e As KeyEventArgs) Handles T112.KeyUp
        If e.KeyCode = Keys.Enter Then T105.Focus()
    End Sub
    Private Sub T105_KeyUp(sender As Object, e As KeyEventArgs) Handles T105.KeyUp
        If e.KeyCode = Keys.Enter Then T113.Focus()
    End Sub
    Private Sub T113_KeyUp(sender As Object, e As KeyEventArgs) Handles T113.KeyUp
        If e.KeyCode = Keys.Enter Then T106.Focus()
    End Sub
    Private Sub T106_KeyUp(sender As Object, e As KeyEventArgs) Handles T106.KeyUp
        If e.KeyCode = Keys.Enter Then T114.Focus()
    End Sub
    Private Sub T114_KeyUp(sender As Object, e As KeyEventArgs) Handles T114.KeyUp
        If e.KeyCode = Keys.Enter Then T107.Focus()
    End Sub
    Private Sub T107_KeyUp(sender As Object, e As KeyEventArgs) Handles T107.KeyUp
        If e.KeyCode = Keys.Enter Then T115.Focus()
    End Sub
    Private Sub T115_KeyUp(sender As Object, e As KeyEventArgs) Handles T115.KeyUp
        If e.KeyCode = Keys.Enter Then T108.Focus()
    End Sub
    Private Sub T108_KeyUp(sender As Object, e As KeyEventArgs) Handles T108.KeyUp
        If e.KeyCode = Keys.Enter Then T116.Focus()
    End Sub
    Private Sub T116_KeyUp(sender As Object, e As KeyEventArgs) Handles T116.KeyUp
        If e.KeyCode = Keys.Enter Then C2.Focus()
    End Sub
    Private Sub C2_KeyUp(sender As Object, e As KeyEventArgs) Handles C2.KeyUp
        If e.KeyCode = Keys.Enter Then TextBox31.Focus()
    End Sub
    Private Sub TextBox31_KeyUp(sender As Object, e As KeyEventArgs) Handles TextBox31.KeyUp
        If e.KeyCode = Keys.Enter Then TextBox1.Focus()
    End Sub


    '================================================
    '   STARTボタン
    '================================================
    Private Sub Label73_DoubleClick(sender As Object, e As EventArgs) Handles Label_START.DoubleClick
        If PicTEST.Visible = True Then Exit Sub
        If Label_START.Text = "OFF" Then
            Label_START.BackColor = Color.Yellow
            Label_START.Text = "ON"
            Timer1.Enabled = True
            set_Panel_ON(5)
        Else
            Label_START.BackColor = Color.Gray
            Label_START.Text = "OFF"
            Timer1.Enabled = False
            Timer2.Enabled = False
            set_Panel_OFF(5)
        End If
    End Sub


    '================================================
    '   STARTボタン点滅用タイマ
    '================================================
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If PicTEST.Visible = True Then Exit Sub
        Label_START.BackColor = Color.Yellow
        Timer2.Enabled = True
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Label_START.BackColor = Color.Gray
        Timer2.Enabled = False
    End Sub


    '================================================
    '   PUMP
    '================================================
    Private Sub Label24_DoubleClick(sender As Object, e As EventArgs) Handles Label24.DoubleClick
        If PicRUN.Visible = True Then Exit Sub
        If Label24.Text = "OFF" Then
            Label24.BackColor = Color.Yellow
            Label24.Text = "ON"
            set_Panel_ON(9)
        Else
            Label24.BackColor = Color.Gray
            Label24.Text = "OFF"
            set_Panel_OFF(9)
        End If
    End Sub

    '================================================
    '   PDJE 切り替え
    '================================================
    Private Sub Label79_DoubleClick(sender As Object, e As EventArgs) Handles Label79.DoubleClick
        If Label79.BackColor = Color.DarkGreen Then
            Label79.BackColor = Color.Lime
            set_Panel_ON(6)
        Else
            Label79.BackColor = Color.DarkGreen
            set_Panel_OFF(6)
        End If
    End Sub


    '================================================
    '   PDRE 切り替え
    '================================================
    Private Sub Label80_DoubleClick(sender As Object, e As EventArgs) Handles Label80.DoubleClick
        If Label80.BackColor = Color.DarkGreen Then
            Label80.BackColor = Color.Lime
            set_Panel_ON(7)
        Else
            Label80.BackColor = Color.DarkGreen
            set_Panel_OFF(7)
        End If
    End Sub


    '================================================
    '   RUNモードからTESTモードに切り替え
    '================================================
    Private Sub PicRUN_Click(sender As Object, e As EventArgs) Handles PicRUN.Click
        PicTEST.Visible = True
        PicRUN.Visible = False
        Label76.BackColor = Color.Cyan
        Label45.BackColor = Color.Cyan
        Label78.BackColor = Color.Lime
        Label77.BackColor = Color.Yellow
        If Label_START.Text = "ON" Then
            Timer1.Enabled = False
            Timer2.Enabled = False
            Label_START.BackColor = Color.Gray
            Label_START.Text = "OFF"
            set_Panel_OFF(5)
        End If
        Timer3.Enabled = False
        Pic_EMMR1.Visible = False
        Pic_EMMR2.Visible = False
        set_Panel_OFF(0)
    End Sub


    '================================================
    '   TESTモードからRUNモードに切り替え
    '================================================
    Private Sub PicTEST_Click(sender As Object, e As EventArgs) Handles PicTEST.Click
        PicRUN.Visible = True
        PicTEST.Visible = False
        Label76.BackColor = Color.MidnightBlue
        Label45.BackColor = Color.MidnightBlue
        Label78.BackColor = Color.DarkGreen
        Label77.BackColor = Color.Gray
        Pic_EMMR1.Visible = False
        Pic_EMMR2.Visible = True
        set_Panel_ON(0)
    End Sub


    '================================================
    '   AIR-A
    '================================================
    Private Sub Label76_MouseDown(sender As Object, e As MouseEventArgs) Handles Label76.MouseDown
        If PicRUN.Visible = True Then Exit Sub
        Label76.BackColor = Color.MidnightBlue
        Label76.Text = "ON"
        set_Panel_ON(1)
    End Sub
    Private Sub Label76_MouseUp(sender As Object, e As MouseEventArgs) Handles Label76.MouseUp
        If PicRUN.Visible = True Then Exit Sub
        Label76.BackColor = Color.Cyan
        Label76.Text = "OFF"
        set_Panel_OFF(1)
    End Sub


    '================================================
    '   AIR-B
    '================================================
    Private Sub Label45_MouseDown(sender As Object, e As MouseEventArgs) Handles Label45.MouseDown
        If PicRUN.Visible = True Then Exit Sub
        Label45.BackColor = Color.MidnightBlue
        Label45.Text = "ON"
        set_Panel_ON(2)
    End Sub
    Private Sub Label45_MouseUp(sender As Object, e As MouseEventArgs) Handles Label45.MouseUp
        If PicRUN.Visible = True Then Exit Sub
        Label45.BackColor = Color.Cyan
        Label45.Text = "OFF"
        set_Panel_OFF(2)
    End Sub


    '================================================
    '   N2
    '================================================
    Private Sub Label78_MouseDown(sender As Object, e As MouseEventArgs) Handles Label78.MouseDown
        If PicRUN.Visible = True Then Exit Sub
        Label78.BackColor = Color.DarkGreen
        Label78.Text = "ON"
        set_Panel_ON(3)
    End Sub
    Private Sub Label78_MouseUp(sender As Object, e As MouseEventArgs) Handles Label78.MouseUp
        If PicRUN.Visible = True Then Exit Sub
        Label78.BackColor = Color.Lime
        Label78.Text = "OFF"
        set_Panel_OFF(3)
    End Sub


    '================================================
    '   LPG
    '================================================
    Private Sub Label77_MouseDown(sender As Object, e As MouseEventArgs) Handles Label77.MouseDown
        If PicRUN.Visible = True Then Exit Sub
        Label77.BackColor = Color.Gray
        Label77.Text = "ON"
        set_Panel_ON(4)
    End Sub
    Private Sub Label77_MouseUp(sender As Object, e As MouseEventArgs) Handles Label77.MouseUp
        If PicRUN.Visible = True Then Exit Sub
        Label77.BackColor = Color.Yellow
        Label77.Text = "OFF"
        set_Panel_OFF(4)
    End Sub


    '================================================
    '   エマージェンシーボタン点滅
    '================================================
    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        If Pic_EMMR1.Visible = False Then
            Pic_EMMR1.Visible = True
            Pic_EMMR2.Visible = False
        Else
            Pic_EMMR2.Visible = True
            Pic_EMMR1.Visible = False
        End If
    End Sub


    '================================================
    '   エマージェンシボタン押下
    '================================================
    Private Sub Pic_EMMR1_Click(sender As Object, e As EventArgs) Handles Pic_EMMR1.Click
        Timer3.Enabled = True
        set_Panel_OFF(0)
    End Sub
    Private Sub Pic_EMMR2_Click(sender As Object, e As EventArgs) Handles Pic_EMMR2.Click
        Timer3.Enabled = True
        set_Panel_OFF(0)
    End Sub


    '================================================
    '   パネル設定
    '================================================
    Private Sub set_Panel_ON(a As Integer)
        Select Case a
            Case 0
                If (xx_Panel And CByte(1)) <> CByte(1) Then xx_Panel = xx_Panel + 1
            Case 1
                If (xx_Panel And CByte(2)) <> CByte(2) Then xx_Panel = xx_Panel + 2
            Case 2
                If (xx_Panel And CByte(4)) <> CByte(4) Then xx_Panel = xx_Panel + 4
            Case 3
                If (xx_Panel And CByte(8)) <> CByte(8) Then xx_Panel = xx_Panel + 8
            Case 4
                If (xx_Panel And CByte(16)) <> CByte(16) Then xx_Panel = xx_Panel + 16
            Case 5
                If (xx_Panel And CByte(32)) <> CByte(32) Then xx_Panel = xx_Panel + 32
            Case 6
                If (xx_Panel And CByte(64)) <> CByte(64) Then xx_Panel = xx_Panel + 64
            Case 7
                If (xx_Panel And CByte(128)) <> CByte(128) Then xx_Panel = xx_Panel + 128
            Case 9
                Label_BIT2.Text = "〇〇〇〇〇〇〇●"
        End Select
        If xx_Panel > 255 Then xx_Panel = 255
        xx_Disp()
    End Sub
    Private Sub set_Panel_OFF(a As Integer)
        Select Case a
            Case 0
                If (xx_Panel And CByte(1)) = CByte(1) Then xx_Panel = xx_Panel - 1
            Case 1
                If (xx_Panel And CByte(2)) = CByte(2) Then xx_Panel = xx_Panel - 2
            Case 2
                If (xx_Panel And CByte(4)) = CByte(4) Then xx_Panel = xx_Panel - 4
            Case 3
                If (xx_Panel And CByte(8)) = CByte(8) Then xx_Panel = xx_Panel - 8
            Case 4
                If (xx_Panel And CByte(16)) = CByte(16) Then xx_Panel = xx_Panel - 16
            Case 5
                If (xx_Panel And CByte(32)) = CByte(32) Then xx_Panel = xx_Panel - 32
            Case 6
                If (xx_Panel And CByte(64)) = CByte(64) Then xx_Panel = xx_Panel - 64
            Case 7
                If (xx_Panel And CByte(128)) = CByte(128) Then xx_Panel = xx_Panel - 128
            Case 9
                Label_BIT2.Text = "〇〇〇〇〇〇〇〇"
        End Select
        If xx_Panel < 0 Then xx_Panel = 0
        xx_Disp()
    End Sub
    Private Sub xx_Disp()
        Dim xdat = ""
        If (xx_Panel And CByte(128)) = CByte(128) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(64)) = CByte(64) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(32)) = CByte(32) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(16)) = CByte(16) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(8)) = CByte(8) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(4)) = CByte(4) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(2)) = CByte(2) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        If (xx_Panel And CByte(1)) = CByte(1) Then
            xdat = xdat + "●"
        Else
            xdat = xdat + "〇"
        End If
        Label_BIT.Text = xdat
    End Sub


    '================================================
    '   パラメータ送信　ＪＥＴ－ＭＯＤＥ
    '================================================
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If Para_Check() Then
            'メッセージボックスを表示する 
            Dim result As DialogResult = MessageBox.Show("上下限値範囲外がありますが、送信してよろしいですか？",
                                             "質問",
                                             MessageBoxButtons.YesNoCancel,
                                             MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button2)

            '何が選択されたか調べる 
            If result = DialogResult.Yes Then
                '「はい」が選択された時 
            ElseIf result = DialogResult.No Then
                '「いいえ」が選択された時 
                Exit Sub
            ElseIf result = DialogResult.Cancel Then
                '「キャンセル」が選択された時 
                Exit Sub
            End If
        End If

        Com_SendAI("A", T2.Text, T10.Text)
        Com_SendAI("B", T3.Text, T11.Text)
        Com_SendAI("C", T4.Text, T12.Text)
        Com_SendAI("D", T5.Text, T13.Text)
        Com_SendAI("E", T6.Text, T14.Text)
        Com_SendAI("F", T7.Text, T15.Text)
        Com_SendAI("G", T8.Text, T16.Text)
        Com_SendAI("H", C1.Text, "")
        Com_SendAI("I", C2.Text, "")

        MsgBox("送信しました。", MessageBoxIcon.Information, "実行")
        'MsgBox("送信しました（ダミー）",, "実行")

    End Sub


    '================================================
    '   パラメータ送信　ＲＯＣＫＥＴ－ＭＯＤＥ
    '================================================
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Para_Check() Then
            'メッセージボックスを表示する 
            Dim result As DialogResult = MessageBox.Show("上下限値範囲外がありますが、送信してよろしいですか？",
                                             "質問",
                                             MessageBoxButtons.YesNoCancel,
                                             MessageBoxIcon.Exclamation,
                                             MessageBoxDefaultButton.Button2)

            '何が選択されたか調べる 
            If result = DialogResult.Yes Then
                '「はい」が選択された時 
            ElseIf result = DialogResult.No Then
                '「いいえ」が選択された時 
                Exit Sub
            ElseIf result = DialogResult.Cancel Then
                '「キャンセル」が選択された時 
                Exit Sub
            End If
        End If

        Com_SendAI("A", T102.Text, T110.Text)
        Com_SendAI("B", T103.Text, T111.Text)
        Com_SendAI("C", T104.Text, T112.Text)
        Com_SendAI("D", T105.Text, T113.Text)
        Com_SendAI("E", T106.Text, T114.Text)
        Com_SendAI("F", T107.Text, T115.Text)
        Com_SendAI("G", T108.Text, T116.Text)
        Com_SendAI("I", C2.Text, "")

        MsgBox("送信しました。", MessageBoxIcon.Information, "実行")
        'MsgBox("送信しました（ダミー）",, "実行")
    End Sub


    '================================================
    '   パラメータチェック
    '================================================
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If Para_Check() Then
            MsgBox("赤い部分をご確認ください。", MessageBoxIcon.Error, "範囲外あり")
        Else
            MsgBox("全て範囲内です。", MessageBoxIcon.Information, "範囲外なし")
        End If
    End Sub

    Private Function Para_Check()
        Dim fileno As Integer
        Dim xdat2 As String
        Dim arr() As String

        Para_Check = 0

        If System.IO.File.Exists("SETTEI.txt") Then
            fileno = FreeFile()
            FileOpen(fileno, "SETTEI.txt", OpenMode.Input)
            xdat2 = LineInput(fileno)   'ファイルから読み込み
            arr = xdat2.Split(",")
            FileClose(fileno)
        Else
            MsgBox("先に設定画面で上下限値を保存してください", MessageBoxIcon.Stop, "警告")
            Exit Function
        End If

        a1 = Val(arr(14))
        a2 = Val(arr(15))
        a3 = Val(arr(16))
        a4 = Val(arr(17))

        For i = 2 To 8
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        For i = 10 To 16
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        C1.BackColor = Color.White
        For i = 102 To 108
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        For i = 110 To 116
            CType(Me.Controls("T" & i), TextBox).BackColor = Color.White
        Next
        C2.BackColor = Color.White

        For i = 2 To 8
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        For i = 10 To 16
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        If In_CheckCNT(C1.Text) Then
            C1.BackColor = Color.Red
            Para_Check = 1
        End If
        For i = 102 To 108
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        For i = 110 To 116
            If In_CheckTIM(CType(Me.Controls("T" & i), TextBox).Text) Then
                CType(Me.Controls("T" & i), TextBox).BackColor = Color.Red
                Para_Check = 1
            End If
        Next
        If In_CheckCNT(C2.Text) Then
            C2.BackColor = Color.Red
            Para_Check = 1
        End If

    End Function

    Private Function In_CheckTIM(xdat As String)
        Dim a As Double
        Dim ydat As String

        In_CheckTIM = 0
        If InStr(xdat, ".") > 0 Then
            ydat = Mid(xdat, InStr(xdat, ".") + 1)
            If Len(ydat) <> 1 Then
                In_CheckTIM = 1
                Exit Function
            End If
        End If
        a = Val(xdat)
        If a < a1 Then
            In_CheckTIM = 1
            Exit Function
        End If
        If a > a2 Then
            In_CheckTIM = 1
            Exit Function
        End If
    End Function

    Private Function In_CheckCNT(xdat As String)
        Dim a As Double

        In_CheckCNT = 0
        If InStr(xdat, ".") > 0 Then
            In_CheckCNT = 1
            Exit Function
        End If
        a = Val(xdat)
        If a < a3 Then
            In_CheckCNT = 1
            Exit Function
        End If
        If a > a4 Then
            In_CheckCNT = 1
            Exit Function
        End If
    End Function




    '================================================
    '   シリアルポートオープン
    '================================================
    Private Sub Com_Open()

        If SerialPort1.IsOpen = True Then

            'シリアルポートをクローズする.
            SerialPort1.Close()

            'ボタンの表示を[切断]から[接続]に変える.
            ConnectButton.Text = "接続"
        Else

            'オープンするシリアルポートをコンボボックスから取り出す.
            SerialPort1.PortName = Form_SETTEI.T2.SelectedItem.ToString()

            'ボーレートをコンボボックスから取り出す.
            Dim baud As BuadRateItem
            baud = Form_SETTEI.T3.SelectedItem
            SerialPort1.BaudRate = baud.BAUDRATE

            'データビットをセットする. (データビット = 8ビット)
            SerialPort1.DataBits = 8

            'パリティビットをセットする. (パリティビット = なし)
            SerialPort1.Parity = Parity.None

            'ストップビットをセットする. (ストップビット = 1ビット)
            SerialPort1.StopBits = StopBits.One

            '文字コードをセットする.
            SerialPort1.Encoding = Encoding.Unicode

            Try
                'シリアルポートをオープンする.
                SerialPort1.Open()

                'ボタンの表示を[接続]から[切断]に変える.
                ConnectButton.Text = "切断"
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If

    End Sub


    '================================================
    '   パラメータ送信　Ａ－Ｉ
    '================================================
    Private Sub Com_SendAI(xdat As String, ydat As String, zdat As String)
        'シリアルポートをオープンしていない場合、処理を行わない.
        If SerialPort1.IsOpen = False Then
            Return
        End If

        'テキストボックスから、送信するテキストを取り出す.
        Dim data As String = ""
        Select Case xdat
            Case "H", "I"
                xdat = CByte(&H2) + xdat + CByte(&H2) + vbCr
            Case Else
                xdat = CByte(&H2) + xdat + CByte(&H4) + vbCr
        End Select

        '送信するテキストがない場合、データ送信は行わない.
        If String.IsNullOrEmpty(data) Then
            Return
        End If

        Try
            'シリアルポートからテキストを送信する.
            SerialPort1.Write(data)

            '送信データを入力するテキストボックスをクリアする.
            SndTextBox.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    '================================================
    '   シリアル送信
    '================================================
    Private Sub Com_Send()
        'シリアルポートをオープンしていない場合、処理を行わない.
        If SerialPort1.IsOpen = False Then
            Return
        End If

        'テキストボックスから、送信するテキストを取り出す.
        Dim data As String
        data = SndTextBox.Text + vbCr '&hODを追加

        '送信するテキストがない場合、データ送信は行わない.
        If String.IsNullOrEmpty(data) Then
            Return
        End If

        Try
            'シリアルポートからテキストを送信する.
            SerialPort1.WriteLine(data)

            '送信データを入力するテキストボックスをクリアする.
            SndTextBox.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    '================================================
    '   シリアル受信
    '       buffer      受信データを格納するバイト型の配列。
    '       offset	    受信データの格納を開始する配列の位置。
    '       count	    受信データを読み込むバイト数。
    '================================================
    Private Sub Com_Recived(buffer As Byte(), offset As Integer, count As Integer)
        'シリアルポートをオープンしていない場合、処理を行わない.
        If SerialPort1.IsOpen = False Then
            Return
        End If

        Try
            '受信データを読み込む.
            Dim data As String
            'data = SerialPort1.ReadExisting()
            data = SerialPort1.ReadLine()

            '受信したデータをテキストボックスに書き込む.
            Me.Label27.Text = data

            'Dim args(0) As Object
            'args(0) = data
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    '================================================
    '   接続・切断　ボタン
    '================================================
    Private Sub ConnectButton_Click(sender As Object, e As EventArgs) Handles ConnectButton.Click

        If SerialPort1.IsOpen = True Then
            'もし接続中なら切断する

            'シリアルポートをクローズする.
            SerialPort1.Close()

            'ボタンの表示を[切断]から[接続]に変える.
            ConnectButton.Text = "接続"
        Else
            'もし切断中なら接続する

            'オープンするシリアルポートをコンボボックスから取り出す.
            SerialPort1.PortName = Form_SETTEI.T2.SelectedItem.ToString()
            'デリミタとなる文字列（改行コード）を設定
            SerialPort1.NewLine = Chr(13)

            'データビットをセットする
            SerialPort1.DataBits = Form_SETTEI.T4.Text

            'パリティビットをセットする
            Select Case Form_SETTEI.T5.SelectedIndex
                Case 0
                    SerialPort1.Parity = Parity.None
                Case 1
                    SerialPort1.Parity = Parity.Odd
                Case 2
                    SerialPort1.Parity = Parity.Even
                Case 3
                    SerialPort1.Parity = Parity.Mark
                Case 4
                    SerialPort1.Parity = Parity.Space
            End Select

            'ストップビットをセットする. (ストップビット = 1ビット)
            Select Case Form_SETTEI.T6.SelectedIndex
                Case 0
                    SerialPort1.StopBits = StopBits.None
                Case 1
                    SerialPort1.StopBits = StopBits.One
                Case 2
                    SerialPort1.StopBits = StopBits.OnePointFive
                Case 3
                    SerialPort1.StopBits = StopBits.Two
            End Select

            '文字コードをセットする.
            SerialPort1.Encoding = Encoding.ASCII

            Try
                'シリアルポートをオープンする.
                SerialPort1.Open()

                'ボタンの表示を[接続]から[切断]に変える.
                ConnectButton.Text = "切断"
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            '[m]を待つ？

            '[M]を送る？

        End If


    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click

        If SerialPort1.IsOpen = False Then
            Return
        End If

        Button10.Text = "受信待ち中"

        ' Timer4を利用しない（デリゲート宣言したサブルーチンで処理する）
        ' Timer4.Enabled = True
        Timer4.Enabled = False

    End Sub

    Private Sub Timer4_Tick(sender As Object, e As EventArgs) Handles Timer4.Tick
        Dim Buffer As Byte()
        Dim offset As Integer = 0
        Dim count As Integer = 0
        Dim R_dat As String

        Com_Recived(Buffer, offset, count)

        If Len(Buffer) = Nothing Then Exit Sub

        R_dat = BitConverter.ToString(Buffer)

        Label27.Text = R_dat

        Button10.Text = "受信"
        Timer4.Enabled = False

    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Com_Send()
    End Sub

    Private Sub SerialPort1_DataReceived(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived

        'DisplayText()をデリゲート宣言
        Dim dlg As New DisplayTextDelegate(AddressOf DisplayText)
        'ポートオープン時に、SerialPort1.NewLine = Chr(13)で指定したデリミタ文字列（改行コード）までを受信
        Dim str As String = SerialPort1.ReadLine()
        '受信が完了したらデリゲート宣言されたサブルーチンを呼び出す
        Me.Invoke(dlg, New Object() {str})

    End Sub

    'Invokeメソッドで使用するデリゲート宣言  
    Delegate Sub DisplayTextDelegate(ByVal strDisp As String)

    Private Sub DisplayText(ByVal strDisp As String)
        '受信したデータをテキストボックスに書き込む
        Me.Label27.Text = strDisp
    End Sub

End Class